﻿namespace program
{
    partial class Cutting
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Cutting));
            this.guna2Panel1 = new Guna.UI2.WinForms.Guna2Panel();
            this.guna2Panel5 = new Guna.UI2.WinForms.Guna2Panel();
            this.guna2CustomCheckBox3 = new Guna.UI2.WinForms.Guna2CustomCheckBox();
            this.guna2CustomCheckBox2 = new Guna.UI2.WinForms.Guna2CustomCheckBox();
            this.guna2PictureBox2 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.guna2ComboBox9 = new Guna.UI2.WinForms.Guna2ComboBox();
            this.guna2HtmlLabel19 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2ComboBox6 = new Guna.UI2.WinForms.Guna2ComboBox();
            this.guna2HtmlLabel18 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2ComboBox5 = new Guna.UI2.WinForms.Guna2ComboBox();
            this.guna2HtmlLabel10 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel11 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel12 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel13 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel6 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel7 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2ComboBox3 = new Guna.UI2.WinForms.Guna2ComboBox();
            this.guna2HtmlLabel8 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel9 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2ComboBox4 = new Guna.UI2.WinForms.Guna2ComboBox();
            this.guna2HtmlLabel4 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel5 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2ComboBox2 = new Guna.UI2.WinForms.Guna2ComboBox();
            this.guna2HtmlLabel17 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel16 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2ComboBox8 = new Guna.UI2.WinForms.Guna2ComboBox();
            this.guna2Button3 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Button2 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2PictureBox1 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.guna2HtmlLabel1 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.nTabControl1 = new Nevron.Nov.WinFormControls.NTabControl();
            this.guna2Panel1.SuspendLayout();
            this.guna2Panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // guna2Panel1
            // 
            this.guna2Panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(240)))), ((int)(((byte)(253)))));
            this.guna2Panel1.Controls.Add(this.guna2Panel5);
            this.guna2Panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.guna2Panel1.Location = new System.Drawing.Point(0, 0);
            this.guna2Panel1.Name = "guna2Panel1";
            this.guna2Panel1.ShadowDecoration.Parent = this.guna2Panel1;
            this.guna2Panel1.Size = new System.Drawing.Size(385, 645);
            this.guna2Panel1.TabIndex = 0;
            // 
            // guna2Panel5
            // 
            this.guna2Panel5.Controls.Add(this.guna2CustomCheckBox3);
            this.guna2Panel5.Controls.Add(this.guna2CustomCheckBox2);
            this.guna2Panel5.Controls.Add(this.guna2PictureBox2);
            this.guna2Panel5.Controls.Add(this.guna2ComboBox9);
            this.guna2Panel5.Controls.Add(this.guna2HtmlLabel19);
            this.guna2Panel5.Controls.Add(this.guna2ComboBox6);
            this.guna2Panel5.Controls.Add(this.guna2HtmlLabel18);
            this.guna2Panel5.Controls.Add(this.guna2ComboBox5);
            this.guna2Panel5.Controls.Add(this.guna2HtmlLabel10);
            this.guna2Panel5.Controls.Add(this.guna2HtmlLabel11);
            this.guna2Panel5.Controls.Add(this.guna2HtmlLabel12);
            this.guna2Panel5.Controls.Add(this.guna2HtmlLabel13);
            this.guna2Panel5.Controls.Add(this.guna2HtmlLabel6);
            this.guna2Panel5.Controls.Add(this.guna2HtmlLabel7);
            this.guna2Panel5.Controls.Add(this.guna2ComboBox3);
            this.guna2Panel5.Controls.Add(this.guna2HtmlLabel8);
            this.guna2Panel5.Controls.Add(this.guna2HtmlLabel9);
            this.guna2Panel5.Controls.Add(this.guna2ComboBox4);
            this.guna2Panel5.Controls.Add(this.guna2HtmlLabel4);
            this.guna2Panel5.Controls.Add(this.guna2HtmlLabel5);
            this.guna2Panel5.Controls.Add(this.guna2ComboBox2);
            this.guna2Panel5.Controls.Add(this.guna2HtmlLabel17);
            this.guna2Panel5.Controls.Add(this.guna2HtmlLabel16);
            this.guna2Panel5.Controls.Add(this.guna2ComboBox8);
            this.guna2Panel5.Controls.Add(this.guna2Button3);
            this.guna2Panel5.Controls.Add(this.guna2Button2);
            this.guna2Panel5.Controls.Add(this.guna2PictureBox1);
            this.guna2Panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.guna2Panel5.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(240)))), ((int)(((byte)(253)))));
            this.guna2Panel5.Location = new System.Drawing.Point(0, 0);
            this.guna2Panel5.Name = "guna2Panel5";
            this.guna2Panel5.ShadowDecoration.Parent = this.guna2Panel5;
            this.guna2Panel5.Size = new System.Drawing.Size(385, 645);
            this.guna2Panel5.TabIndex = 3;
            // 
            // guna2CustomCheckBox3
            // 
            this.guna2CustomCheckBox3.CheckedState.BorderColor = System.Drawing.Color.PaleGreen;
            this.guna2CustomCheckBox3.CheckedState.BorderRadius = 1;
            this.guna2CustomCheckBox3.CheckedState.BorderThickness = 1;
            this.guna2CustomCheckBox3.CheckedState.FillColor = System.Drawing.Color.White;
            this.guna2CustomCheckBox3.CheckedState.Parent = this.guna2CustomCheckBox3;
            this.guna2CustomCheckBox3.CheckMarkColor = System.Drawing.Color.OliveDrab;
            this.guna2CustomCheckBox3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.guna2CustomCheckBox3.Location = new System.Drawing.Point(69, 76);
            this.guna2CustomCheckBox3.Name = "guna2CustomCheckBox3";
            this.guna2CustomCheckBox3.ShadowDecoration.Parent = this.guna2CustomCheckBox3;
            this.guna2CustomCheckBox3.Size = new System.Drawing.Size(40, 37);
            this.guna2CustomCheckBox3.TabIndex = 35;
            this.guna2CustomCheckBox3.UncheckedState.BorderColor = System.Drawing.Color.Black;
            this.guna2CustomCheckBox3.UncheckedState.BorderRadius = 1;
            this.guna2CustomCheckBox3.UncheckedState.BorderThickness = 1;
            this.guna2CustomCheckBox3.UncheckedState.FillColor = System.Drawing.Color.White;
            this.guna2CustomCheckBox3.UncheckedState.Parent = this.guna2CustomCheckBox3;
            // 
            // guna2CustomCheckBox2
            // 
            this.guna2CustomCheckBox2.CheckedState.BorderColor = System.Drawing.Color.PaleGreen;
            this.guna2CustomCheckBox2.CheckedState.BorderRadius = 1;
            this.guna2CustomCheckBox2.CheckedState.BorderThickness = 1;
            this.guna2CustomCheckBox2.CheckedState.FillColor = System.Drawing.Color.White;
            this.guna2CustomCheckBox2.CheckedState.Parent = this.guna2CustomCheckBox2;
            this.guna2CustomCheckBox2.CheckMarkColor = System.Drawing.Color.OliveDrab;
            this.guna2CustomCheckBox2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.guna2CustomCheckBox2.Location = new System.Drawing.Point(66, 307);
            this.guna2CustomCheckBox2.Name = "guna2CustomCheckBox2";
            this.guna2CustomCheckBox2.ShadowDecoration.Parent = this.guna2CustomCheckBox2;
            this.guna2CustomCheckBox2.Size = new System.Drawing.Size(40, 37);
            this.guna2CustomCheckBox2.TabIndex = 34;
            this.guna2CustomCheckBox2.UncheckedState.BorderColor = System.Drawing.Color.Black;
            this.guna2CustomCheckBox2.UncheckedState.BorderRadius = 1;
            this.guna2CustomCheckBox2.UncheckedState.BorderThickness = 1;
            this.guna2CustomCheckBox2.UncheckedState.FillColor = System.Drawing.Color.White;
            this.guna2CustomCheckBox2.UncheckedState.Parent = this.guna2CustomCheckBox2;
            // 
            // guna2PictureBox2
            // 
            this.guna2PictureBox2.Image = global::program.Properties.Resources._1;
            this.guna2PictureBox2.Location = new System.Drawing.Point(170, 36);
            this.guna2PictureBox2.Name = "guna2PictureBox2";
            this.guna2PictureBox2.ShadowDecoration.Parent = this.guna2PictureBox2;
            this.guna2PictureBox2.Size = new System.Drawing.Size(194, 184);
            this.guna2PictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.guna2PictureBox2.TabIndex = 31;
            this.guna2PictureBox2.TabStop = false;
            // 
            // guna2ComboBox9
            // 
            this.guna2ComboBox9.BackColor = System.Drawing.Color.Transparent;
            this.guna2ComboBox9.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.guna2ComboBox9.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.guna2ComboBox9.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox9.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox9.FocusedState.Parent = this.guna2ComboBox9;
            this.guna2ComboBox9.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.guna2ComboBox9.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.guna2ComboBox9.HoverState.Parent = this.guna2ComboBox9;
            this.guna2ComboBox9.ItemHeight = 25;
            this.guna2ComboBox9.ItemsAppearance.Parent = this.guna2ComboBox9;
            this.guna2ComboBox9.Location = new System.Drawing.Point(230, 496);
            this.guna2ComboBox9.Name = "guna2ComboBox9";
            this.guna2ComboBox9.ShadowDecoration.Color = System.Drawing.Color.Gray;
            this.guna2ComboBox9.ShadowDecoration.Enabled = true;
            this.guna2ComboBox9.ShadowDecoration.Parent = this.guna2ComboBox9;
            this.guna2ComboBox9.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 4, 4);
            this.guna2ComboBox9.Size = new System.Drawing.Size(80, 31);
            this.guna2ComboBox9.TabIndex = 29;
            // 
            // guna2HtmlLabel19
            // 
            this.guna2HtmlLabel19.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel19.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel19.Location = new System.Drawing.Point(182, 496);
            this.guna2HtmlLabel19.Name = "guna2HtmlLabel19";
            this.guna2HtmlLabel19.Size = new System.Drawing.Size(42, 27);
            this.guna2HtmlLabel19.TabIndex = 26;
            this.guna2HtmlLabel19.Text = "q1 =";
            // 
            // guna2ComboBox6
            // 
            this.guna2ComboBox6.BackColor = System.Drawing.Color.Transparent;
            this.guna2ComboBox6.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.guna2ComboBox6.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.guna2ComboBox6.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox6.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox6.FocusedState.Parent = this.guna2ComboBox6;
            this.guna2ComboBox6.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.guna2ComboBox6.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.guna2ComboBox6.HoverState.Parent = this.guna2ComboBox6;
            this.guna2ComboBox6.ItemHeight = 25;
            this.guna2ComboBox6.ItemsAppearance.Parent = this.guna2ComboBox6;
            this.guna2ComboBox6.Location = new System.Drawing.Point(50, 496);
            this.guna2ComboBox6.Name = "guna2ComboBox6";
            this.guna2ComboBox6.ShadowDecoration.Color = System.Drawing.Color.Gray;
            this.guna2ComboBox6.ShadowDecoration.Enabled = true;
            this.guna2ComboBox6.ShadowDecoration.Parent = this.guna2ComboBox6;
            this.guna2ComboBox6.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 4, 4);
            this.guna2ComboBox6.Size = new System.Drawing.Size(80, 31);
            this.guna2ComboBox6.TabIndex = 28;
            // 
            // guna2HtmlLabel18
            // 
            this.guna2HtmlLabel18.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel18.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel18.Location = new System.Drawing.Point(316, 496);
            this.guna2HtmlLabel18.Name = "guna2HtmlLabel18";
            this.guna2HtmlLabel18.Size = new System.Drawing.Size(57, 27);
            this.guna2HtmlLabel18.TabIndex = 25;
            this.guna2HtmlLabel18.Text = "[кН/м]";
            // 
            // guna2ComboBox5
            // 
            this.guna2ComboBox5.BackColor = System.Drawing.Color.Transparent;
            this.guna2ComboBox5.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.guna2ComboBox5.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.guna2ComboBox5.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox5.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox5.FocusedState.Parent = this.guna2ComboBox5;
            this.guna2ComboBox5.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.guna2ComboBox5.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.guna2ComboBox5.HoverState.Parent = this.guna2ComboBox5;
            this.guna2ComboBox5.ItemHeight = 25;
            this.guna2ComboBox5.ItemsAppearance.Parent = this.guna2ComboBox5;
            this.guna2ComboBox5.Location = new System.Drawing.Point(50, 459);
            this.guna2ComboBox5.Name = "guna2ComboBox5";
            this.guna2ComboBox5.ShadowDecoration.Color = System.Drawing.Color.Gray;
            this.guna2ComboBox5.ShadowDecoration.Enabled = true;
            this.guna2ComboBox5.ShadowDecoration.Parent = this.guna2ComboBox5;
            this.guna2ComboBox5.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 4, 4);
            this.guna2ComboBox5.Size = new System.Drawing.Size(80, 31);
            this.guna2ComboBox5.TabIndex = 27;
            // 
            // guna2HtmlLabel10
            // 
            this.guna2HtmlLabel10.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel10.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel10.Location = new System.Drawing.Point(5, 496);
            this.guna2HtmlLabel10.Name = "guna2HtmlLabel10";
            this.guna2HtmlLabel10.Size = new System.Drawing.Size(42, 27);
            this.guna2HtmlLabel10.TabIndex = 23;
            this.guna2HtmlLabel10.Text = "b1 =";
            // 
            // guna2HtmlLabel11
            // 
            this.guna2HtmlLabel11.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel11.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel11.Location = new System.Drawing.Point(132, 496);
            this.guna2HtmlLabel11.Name = "guna2HtmlLabel11";
            this.guna2HtmlLabel11.Size = new System.Drawing.Size(41, 27);
            this.guna2HtmlLabel11.TabIndex = 22;
            this.guna2HtmlLabel11.Text = "[мм]";
            // 
            // guna2HtmlLabel12
            // 
            this.guna2HtmlLabel12.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel12.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel12.Location = new System.Drawing.Point(8, 459);
            this.guna2HtmlLabel12.Name = "guna2HtmlLabel12";
            this.guna2HtmlLabel12.Size = new System.Drawing.Size(41, 27);
            this.guna2HtmlLabel12.TabIndex = 20;
            this.guna2HtmlLabel12.Text = "a1 =";
            // 
            // guna2HtmlLabel13
            // 
            this.guna2HtmlLabel13.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel13.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel13.Location = new System.Drawing.Point(132, 459);
            this.guna2HtmlLabel13.Name = "guna2HtmlLabel13";
            this.guna2HtmlLabel13.Size = new System.Drawing.Size(41, 27);
            this.guna2HtmlLabel13.TabIndex = 19;
            this.guna2HtmlLabel13.Text = "[мм]";
            // 
            // guna2HtmlLabel6
            // 
            this.guna2HtmlLabel6.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel6.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel6.Location = new System.Drawing.Point(13, 422);
            this.guna2HtmlLabel6.Name = "guna2HtmlLabel6";
            this.guna2HtmlLabel6.Size = new System.Drawing.Size(32, 27);
            this.guna2HtmlLabel6.TabIndex = 17;
            this.guna2HtmlLabel6.Text = "b =";
            // 
            // guna2HtmlLabel7
            // 
            this.guna2HtmlLabel7.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel7.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel7.Location = new System.Drawing.Point(132, 422);
            this.guna2HtmlLabel7.Name = "guna2HtmlLabel7";
            this.guna2HtmlLabel7.Size = new System.Drawing.Size(41, 27);
            this.guna2HtmlLabel7.TabIndex = 16;
            this.guna2HtmlLabel7.Text = "[мм]";
            // 
            // guna2ComboBox3
            // 
            this.guna2ComboBox3.BackColor = System.Drawing.Color.Transparent;
            this.guna2ComboBox3.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.guna2ComboBox3.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.guna2ComboBox3.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox3.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox3.FocusedState.Parent = this.guna2ComboBox3;
            this.guna2ComboBox3.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.guna2ComboBox3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.guna2ComboBox3.HoverState.Parent = this.guna2ComboBox3;
            this.guna2ComboBox3.ItemHeight = 25;
            this.guna2ComboBox3.ItemsAppearance.Parent = this.guna2ComboBox3;
            this.guna2ComboBox3.Location = new System.Drawing.Point(50, 422);
            this.guna2ComboBox3.Name = "guna2ComboBox3";
            this.guna2ComboBox3.ShadowDecoration.Color = System.Drawing.Color.Gray;
            this.guna2ComboBox3.ShadowDecoration.Enabled = true;
            this.guna2ComboBox3.ShadowDecoration.Parent = this.guna2ComboBox3;
            this.guna2ComboBox3.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 4, 4);
            this.guna2ComboBox3.Size = new System.Drawing.Size(80, 31);
            this.guna2ComboBox3.TabIndex = 15;
            // 
            // guna2HtmlLabel8
            // 
            this.guna2HtmlLabel8.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel8.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel8.Location = new System.Drawing.Point(13, 385);
            this.guna2HtmlLabel8.Name = "guna2HtmlLabel8";
            this.guna2HtmlLabel8.Size = new System.Drawing.Size(31, 27);
            this.guna2HtmlLabel8.TabIndex = 14;
            this.guna2HtmlLabel8.Text = "a =";
            // 
            // guna2HtmlLabel9
            // 
            this.guna2HtmlLabel9.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel9.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel9.Location = new System.Drawing.Point(132, 385);
            this.guna2HtmlLabel9.Name = "guna2HtmlLabel9";
            this.guna2HtmlLabel9.Size = new System.Drawing.Size(41, 27);
            this.guna2HtmlLabel9.TabIndex = 13;
            this.guna2HtmlLabel9.Text = "[мм]";
            // 
            // guna2ComboBox4
            // 
            this.guna2ComboBox4.BackColor = System.Drawing.Color.Transparent;
            this.guna2ComboBox4.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.guna2ComboBox4.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.guna2ComboBox4.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox4.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox4.FocusedState.Parent = this.guna2ComboBox4;
            this.guna2ComboBox4.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.guna2ComboBox4.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.guna2ComboBox4.HoverState.Parent = this.guna2ComboBox4;
            this.guna2ComboBox4.ItemHeight = 25;
            this.guna2ComboBox4.ItemsAppearance.Parent = this.guna2ComboBox4;
            this.guna2ComboBox4.Location = new System.Drawing.Point(50, 385);
            this.guna2ComboBox4.Name = "guna2ComboBox4";
            this.guna2ComboBox4.ShadowDecoration.Color = System.Drawing.Color.Gray;
            this.guna2ComboBox4.ShadowDecoration.Enabled = true;
            this.guna2ComboBox4.ShadowDecoration.Parent = this.guna2ComboBox4;
            this.guna2ComboBox4.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 4, 4);
            this.guna2ComboBox4.Size = new System.Drawing.Size(80, 31);
            this.guna2ComboBox4.TabIndex = 12;
            // 
            // guna2HtmlLabel4
            // 
            this.guna2HtmlLabel4.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel4.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel4.Location = new System.Drawing.Point(17, 189);
            this.guna2HtmlLabel4.Name = "guna2HtmlLabel4";
            this.guna2HtmlLabel4.Size = new System.Drawing.Size(32, 27);
            this.guna2HtmlLabel4.TabIndex = 11;
            this.guna2HtmlLabel4.Text = "b =";
            this.guna2HtmlLabel4.Click += new System.EventHandler(this.guna2HtmlLabel4_Click);
            // 
            // guna2HtmlLabel5
            // 
            this.guna2HtmlLabel5.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel5.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel5.Location = new System.Drawing.Point(132, 189);
            this.guna2HtmlLabel5.Name = "guna2HtmlLabel5";
            this.guna2HtmlLabel5.Size = new System.Drawing.Size(41, 27);
            this.guna2HtmlLabel5.TabIndex = 10;
            this.guna2HtmlLabel5.Text = "[мм]";
            this.guna2HtmlLabel5.Click += new System.EventHandler(this.guna2HtmlLabel5_Click);
            // 
            // guna2ComboBox2
            // 
            this.guna2ComboBox2.BackColor = System.Drawing.Color.Transparent;
            this.guna2ComboBox2.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.guna2ComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.guna2ComboBox2.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox2.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox2.FocusedState.Parent = this.guna2ComboBox2;
            this.guna2ComboBox2.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.guna2ComboBox2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.guna2ComboBox2.HoverState.Parent = this.guna2ComboBox2;
            this.guna2ComboBox2.ItemHeight = 25;
            this.guna2ComboBox2.ItemsAppearance.Parent = this.guna2ComboBox2;
            this.guna2ComboBox2.Location = new System.Drawing.Point(50, 189);
            this.guna2ComboBox2.Name = "guna2ComboBox2";
            this.guna2ComboBox2.ShadowDecoration.Color = System.Drawing.Color.Gray;
            this.guna2ComboBox2.ShadowDecoration.Enabled = true;
            this.guna2ComboBox2.ShadowDecoration.Parent = this.guna2ComboBox2;
            this.guna2ComboBox2.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 4, 4);
            this.guna2ComboBox2.Size = new System.Drawing.Size(80, 31);
            this.guna2ComboBox2.TabIndex = 9;
            this.guna2ComboBox2.SelectedIndexChanged += new System.EventHandler(this.guna2ComboBox2_SelectedIndexChanged);
            // 
            // guna2HtmlLabel17
            // 
            this.guna2HtmlLabel17.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel17.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel17.Location = new System.Drawing.Point(16, 154);
            this.guna2HtmlLabel17.Name = "guna2HtmlLabel17";
            this.guna2HtmlLabel17.Size = new System.Drawing.Size(31, 27);
            this.guna2HtmlLabel17.TabIndex = 8;
            this.guna2HtmlLabel17.Text = "a =";
            this.guna2HtmlLabel17.Click += new System.EventHandler(this.guna2HtmlLabel17_Click);
            // 
            // guna2HtmlLabel16
            // 
            this.guna2HtmlLabel16.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel16.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel16.Location = new System.Drawing.Point(132, 154);
            this.guna2HtmlLabel16.Name = "guna2HtmlLabel16";
            this.guna2HtmlLabel16.Size = new System.Drawing.Size(41, 27);
            this.guna2HtmlLabel16.TabIndex = 7;
            this.guna2HtmlLabel16.Text = "[мм]";
            this.guna2HtmlLabel16.Click += new System.EventHandler(this.guna2HtmlLabel16_Click);
            // 
            // guna2ComboBox8
            // 
            this.guna2ComboBox8.BackColor = System.Drawing.Color.Transparent;
            this.guna2ComboBox8.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.guna2ComboBox8.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.guna2ComboBox8.FocusedColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox8.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2ComboBox8.FocusedState.Parent = this.guna2ComboBox8;
            this.guna2ComboBox8.Font = new System.Drawing.Font("Segoe UI", 10F);
            this.guna2ComboBox8.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(68)))), ((int)(((byte)(88)))), ((int)(((byte)(112)))));
            this.guna2ComboBox8.HoverState.Parent = this.guna2ComboBox8;
            this.guna2ComboBox8.ItemHeight = 25;
            this.guna2ComboBox8.ItemsAppearance.Parent = this.guna2ComboBox8;
            this.guna2ComboBox8.Location = new System.Drawing.Point(50, 154);
            this.guna2ComboBox8.Name = "guna2ComboBox8";
            this.guna2ComboBox8.ShadowDecoration.Color = System.Drawing.Color.Gray;
            this.guna2ComboBox8.ShadowDecoration.Enabled = true;
            this.guna2ComboBox8.ShadowDecoration.Parent = this.guna2ComboBox8;
            this.guna2ComboBox8.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 4, 4);
            this.guna2ComboBox8.Size = new System.Drawing.Size(80, 31);
            this.guna2ComboBox8.TabIndex = 6;
            this.guna2ComboBox8.SelectedIndexChanged += new System.EventHandler(this.guna2ComboBox8_SelectedIndexChanged);
            // 
            // guna2Button3
            // 
            this.guna2Button3.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button3.BorderRadius = 5;
            this.guna2Button3.BorderThickness = 1;
            this.guna2Button3.CheckedState.Parent = this.guna2Button3;
            this.guna2Button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.guna2Button3.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.guna2Button3.CustomImages.Parent = this.guna2Button3;
            this.guna2Button3.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.guna2Button3.Font = new System.Drawing.Font("Segoe UI", 20F);
            this.guna2Button3.ForeColor = System.Drawing.Color.Black;
            this.guna2Button3.HoverState.Parent = this.guna2Button3;
            this.guna2Button3.Location = new System.Drawing.Point(110, 586);
            this.guna2Button3.Name = "guna2Button3";
            this.guna2Button3.ShadowDecoration.Color = System.Drawing.Color.Gray;
            this.guna2Button3.ShadowDecoration.Parent = this.guna2Button3;
            this.guna2Button3.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 4, 4);
            this.guna2Button3.Size = new System.Drawing.Size(171, 41);
            this.guna2Button3.TabIndex = 5;
            this.guna2Button3.Text = "Экспорт";
            this.guna2Button3.TextOffset = new System.Drawing.Point(0, -3);
            // 
            // guna2Button2
            // 
            this.guna2Button2.BackColor = System.Drawing.Color.Transparent;
            this.guna2Button2.BorderRadius = 5;
            this.guna2Button2.CheckedState.Parent = this.guna2Button2;
            this.guna2Button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.guna2Button2.CustomImages.Parent = this.guna2Button2;
            this.guna2Button2.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(21)))), ((int)(((byte)(24)))));
            this.guna2Button2.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2Button2.ForeColor = System.Drawing.Color.White;
            this.guna2Button2.HoverState.Parent = this.guna2Button2;
            this.guna2Button2.Location = new System.Drawing.Point(110, 542);
            this.guna2Button2.Name = "guna2Button2";
            this.guna2Button2.ShadowDecoration.Color = System.Drawing.Color.Gray;
            this.guna2Button2.ShadowDecoration.Enabled = true;
            this.guna2Button2.ShadowDecoration.Parent = this.guna2Button2;
            this.guna2Button2.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 4, 4);
            this.guna2Button2.Size = new System.Drawing.Size(171, 41);
            this.guna2Button2.TabIndex = 5;
            this.guna2Button2.Text = "Построить";
            this.guna2Button2.TextOffset = new System.Drawing.Point(0, -3);
            // 
            // guna2PictureBox1
            // 
            this.guna2PictureBox1.Image = global::program.Properties.Resources._2;
            this.guna2PictureBox1.Location = new System.Drawing.Point(160, 258);
            this.guna2PictureBox1.Name = "guna2PictureBox1";
            this.guna2PictureBox1.ShadowDecoration.Parent = this.guna2PictureBox1;
            this.guna2PictureBox1.Size = new System.Drawing.Size(216, 209);
            this.guna2PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.guna2PictureBox1.TabIndex = 32;
            this.guna2PictureBox1.TabStop = false;
            // 
            // guna2HtmlLabel1
            // 
            this.guna2HtmlLabel1.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel1.Font = new System.Drawing.Font("Segoe UI Semibold", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel1.Location = new System.Drawing.Point(659, 8);
            this.guna2HtmlLabel1.Name = "guna2HtmlLabel1";
            this.guna2HtmlLabel1.Size = new System.Drawing.Size(438, 73);
            this.guna2HtmlLabel1.TabIndex = 1;
            this.guna2HtmlLabel1.Text = "Сечение - эллипс";
            // 
            // nTabControl1
            // 
            this.nTabControl1.AutoSize = false;
            this.nTabControl1.DesignTimeState = resources.GetString("nTabControl1.DesignTimeState");
            this.nTabControl1.Location = new System.Drawing.Point(401, 87);
            this.nTabControl1.Name = "nTabControl1";
            this.nTabControl1.Size = new System.Drawing.Size(950, 539);
            this.nTabControl1.TabIndex = 2;
            // 
            // Cutting
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.Controls.Add(this.nTabControl1);
            this.Controls.Add(this.guna2HtmlLabel1);
            this.Controls.Add(this.guna2Panel1);
            this.Name = "Cutting";
            this.Size = new System.Drawing.Size(1366, 645);
            this.guna2Panel1.ResumeLayout(false);
            this.guna2Panel5.ResumeLayout(false);
            this.guna2Panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.guna2PictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Panel guna2Panel1;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel1;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel5;
        private Guna.UI2.WinForms.Guna2Button guna2Button3;
        private Guna.UI2.WinForms.Guna2Button guna2Button2;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel4;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel5;
        private Guna.UI2.WinForms.Guna2ComboBox guna2ComboBox2;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel10;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel11;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel12;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel13;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel6;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel7;
        private Guna.UI2.WinForms.Guna2ComboBox guna2ComboBox3;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel8;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel9;
        private Guna.UI2.WinForms.Guna2ComboBox guna2ComboBox4;
        private Guna.UI2.WinForms.Guna2ComboBox guna2ComboBox6;
        private Guna.UI2.WinForms.Guna2ComboBox guna2ComboBox5;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox2;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel17;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel16;
        private Guna.UI2.WinForms.Guna2ComboBox guna2ComboBox8;
        private Guna.UI2.WinForms.Guna2PictureBox guna2PictureBox1;
        private Guna.UI2.WinForms.Guna2ComboBox guna2ComboBox9;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel19;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel18;
        private Guna.UI2.WinForms.Guna2CustomCheckBox guna2CustomCheckBox3;
        private Guna.UI2.WinForms.Guna2CustomCheckBox guna2CustomCheckBox2;
        private Nevron.Nov.WinFormControls.NTabControl nTabControl1;
    }
}
