﻿namespace program
{
    partial class Fm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.guna2TextBox2 = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2HtmlLabel1 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel2 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2Button1 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2PanelForm = new Guna.UI2.WinForms.Guna2Panel();
            this.guna2ButtonEnt = new Guna.UI2.WinForms.Guna2Button();
            this.guna2ButtonReg = new Guna.UI2.WinForms.Guna2Button();
            this.guna2PanelEnt = new Guna.UI2.WinForms.Guna2Panel();
            this.guna2ButtonFP = new Guna.UI2.WinForms.Guna2Button();
            this.guna2TextBox1 = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2PanelReg = new Guna.UI2.WinForms.Guna2Panel();
            this.guna2TextBox5 = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2TextBox4 = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2TextBox3 = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2Button4 = new Guna.UI2.WinForms.Guna2Button();
            this.guna2HtmlLabel6 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel4 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel3 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2PanelForm.SuspendLayout();
            this.guna2PanelEnt.SuspendLayout();
            this.guna2PanelReg.SuspendLayout();
            this.SuspendLayout();
            // 
            // guna2TextBox2
            // 
            this.guna2TextBox2.Animated = true;
            this.guna2TextBox2.BackColor = System.Drawing.Color.Transparent;
            this.guna2TextBox2.BorderRadius = 5;
            this.guna2TextBox2.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox2.DefaultText = "";
            this.guna2TextBox2.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox2.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox2.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox2.DisabledState.Parent = this.guna2TextBox2;
            this.guna2TextBox2.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox2.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox2.FocusedState.Parent = this.guna2TextBox2;
            this.guna2TextBox2.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2TextBox2.ForeColor = System.Drawing.Color.Black;
            this.guna2TextBox2.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox2.HoverState.Parent = this.guna2TextBox2;
            this.guna2TextBox2.Location = new System.Drawing.Point(128, 111);
            this.guna2TextBox2.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.guna2TextBox2.MaximumSize = new System.Drawing.Size(330, 53);
            this.guna2TextBox2.MinimumSize = new System.Drawing.Size(330, 53);
            this.guna2TextBox2.Name = "guna2TextBox2";
            this.guna2TextBox2.PasswordChar = '\0';
            this.guna2TextBox2.PlaceholderText = "*******";
            this.guna2TextBox2.SelectedText = "";
            this.guna2TextBox2.ShadowDecoration.Parent = this.guna2TextBox2;
            this.guna2TextBox2.Size = new System.Drawing.Size(330, 53);
            this.guna2TextBox2.TabIndex = 2;
            this.guna2TextBox2.TextOffset = new System.Drawing.Point(0, 3);
            // 
            // guna2HtmlLabel1
            // 
            this.guna2HtmlLabel1.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel1.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel1.Location = new System.Drawing.Point(16, 55);
            this.guna2HtmlLabel1.Name = "guna2HtmlLabel1";
            this.guna2HtmlLabel1.Size = new System.Drawing.Size(79, 39);
            this.guna2HtmlLabel1.TabIndex = 14;
            this.guna2HtmlLabel1.Text = "Логин";
            // 
            // guna2HtmlLabel2
            // 
            this.guna2HtmlLabel2.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel2.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel2.Location = new System.Drawing.Point(16, 115);
            this.guna2HtmlLabel2.Name = "guna2HtmlLabel2";
            this.guna2HtmlLabel2.Size = new System.Drawing.Size(96, 39);
            this.guna2HtmlLabel2.TabIndex = 14;
            this.guna2HtmlLabel2.Text = "Пароль";
            // 
            // guna2Button1
            // 
            this.guna2Button1.BorderRadius = 5;
            this.guna2Button1.CheckedState.Parent = this.guna2Button1;
            this.guna2Button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.guna2Button1.CustomImages.Parent = this.guna2Button1;
            this.guna2Button1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(21)))), ((int)(((byte)(24)))));
            this.guna2Button1.Font = new System.Drawing.Font("Segoe UI", 36F);
            this.guna2Button1.ForeColor = System.Drawing.Color.White;
            this.guna2Button1.HoverState.Parent = this.guna2Button1;
            this.guna2Button1.Location = new System.Drawing.Point(16, 256);
            this.guna2Button1.Margin = new System.Windows.Forms.Padding(3, 313, 3, 3);
            this.guna2Button1.Name = "guna2Button1";
            this.guna2Button1.ShadowDecoration.Parent = this.guna2Button1;
            this.guna2Button1.Size = new System.Drawing.Size(453, 77);
            this.guna2Button1.TabIndex = 14;
            this.guna2Button1.Text = "Вход";
            this.guna2Button1.TextOffset = new System.Drawing.Point(0, -3);
            // 
            // guna2PanelForm
            // 
            this.guna2PanelForm.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(21)))), ((int)(((byte)(24)))));
            this.guna2PanelForm.BorderThickness = 1;
            this.guna2PanelForm.Controls.Add(this.guna2ButtonEnt);
            this.guna2PanelForm.Controls.Add(this.guna2ButtonReg);
            this.guna2PanelForm.Controls.Add(this.guna2PanelEnt);
            this.guna2PanelForm.Controls.Add(this.guna2PanelReg);
            this.guna2PanelForm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.guna2PanelForm.Location = new System.Drawing.Point(0, 0);
            this.guna2PanelForm.Name = "guna2PanelForm";
            this.guna2PanelForm.ShadowDecoration.Parent = this.guna2PanelForm;
            this.guna2PanelForm.Size = new System.Drawing.Size(510, 430);
            this.guna2PanelForm.TabIndex = 7;
            // 
            // guna2ButtonEnt
            // 
            this.guna2ButtonEnt.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(245)))), ((int)(((byte)(255)))));
            this.guna2ButtonEnt.CheckedState.Parent = this.guna2ButtonEnt;
            this.guna2ButtonEnt.CustomImages.Parent = this.guna2ButtonEnt;
            this.guna2ButtonEnt.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(245)))), ((int)(((byte)(255)))));
            this.guna2ButtonEnt.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2ButtonEnt.ForeColor = System.Drawing.Color.Black;
            this.guna2ButtonEnt.HoverState.FillColor = System.Drawing.Color.Transparent;
            this.guna2ButtonEnt.HoverState.Parent = this.guna2ButtonEnt;
            this.guna2ButtonEnt.Location = new System.Drawing.Point(311, 12);
            this.guna2ButtonEnt.Name = "guna2ButtonEnt";
            this.guna2ButtonEnt.ShadowDecoration.Parent = this.guna2ButtonEnt;
            this.guna2ButtonEnt.Size = new System.Drawing.Size(111, 45);
            this.guna2ButtonEnt.TabIndex = 13;
            this.guna2ButtonEnt.Text = "Вход";
            this.guna2ButtonEnt.Click += new System.EventHandler(this.guna2ButtonEnt_Click);
            // 
            // guna2ButtonReg
            // 
            this.guna2ButtonReg.AllowDrop = true;
            this.guna2ButtonReg.BackColor = System.Drawing.Color.Transparent;
            this.guna2ButtonReg.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(245)))), ((int)(((byte)(255)))));
            this.guna2ButtonReg.CheckedState.Parent = this.guna2ButtonReg;
            this.guna2ButtonReg.CustomImages.Parent = this.guna2ButtonReg;
            this.guna2ButtonReg.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(245)))), ((int)(((byte)(255)))));
            this.guna2ButtonReg.Font = new System.Drawing.Font("Segoe UI", 24F);
            this.guna2ButtonReg.ForeColor = System.Drawing.Color.Black;
            this.guna2ButtonReg.HoverState.FillColor = System.Drawing.Color.Transparent;
            this.guna2ButtonReg.HoverState.Parent = this.guna2ButtonReg;
            this.guna2ButtonReg.Location = new System.Drawing.Point(68, 12);
            this.guna2ButtonReg.Name = "guna2ButtonReg";
            this.guna2ButtonReg.ShadowDecoration.Parent = this.guna2ButtonReg;
            this.guna2ButtonReg.Size = new System.Drawing.Size(223, 45);
            this.guna2ButtonReg.TabIndex = 12;
            this.guna2ButtonReg.Text = "Регистрация";
            this.guna2ButtonReg.Click += new System.EventHandler(this.guna2ButtonReg_Click);
            // 
            // guna2PanelEnt
            // 
            this.guna2PanelEnt.Controls.Add(this.guna2ButtonFP);
            this.guna2PanelEnt.Controls.Add(this.guna2Button1);
            this.guna2PanelEnt.Controls.Add(this.guna2TextBox1);
            this.guna2PanelEnt.Controls.Add(this.guna2HtmlLabel2);
            this.guna2PanelEnt.Controls.Add(this.guna2HtmlLabel1);
            this.guna2PanelEnt.Controls.Add(this.guna2TextBox2);
            this.guna2PanelEnt.Location = new System.Drawing.Point(13, 57);
            this.guna2PanelEnt.Name = "guna2PanelEnt";
            this.guna2PanelEnt.ShadowDecoration.Parent = this.guna2PanelEnt;
            this.guna2PanelEnt.Size = new System.Drawing.Size(485, 362);
            this.guna2PanelEnt.TabIndex = 14;
            // 
            // guna2ButtonFP
            // 
            this.guna2ButtonFP.CheckedState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(245)))), ((int)(((byte)(255)))));
            this.guna2ButtonFP.CheckedState.Parent = this.guna2ButtonFP;
            this.guna2ButtonFP.Cursor = System.Windows.Forms.Cursors.Hand;
            this.guna2ButtonFP.CustomImages.Parent = this.guna2ButtonFP;
            this.guna2ButtonFP.FillColor = System.Drawing.Color.Transparent;
            this.guna2ButtonFP.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2ButtonFP.ForeColor = System.Drawing.SystemColors.ControlText;
            this.guna2ButtonFP.HoverState.FillColor = System.Drawing.Color.Transparent;
            this.guna2ButtonFP.HoverState.Parent = this.guna2ButtonFP;
            this.guna2ButtonFP.Location = new System.Drawing.Point(237, 171);
            this.guna2ButtonFP.Name = "guna2ButtonFP";
            this.guna2ButtonFP.ShadowDecoration.Parent = this.guna2ButtonFP;
            this.guna2ButtonFP.Size = new System.Drawing.Size(245, 39);
            this.guna2ButtonFP.TabIndex = 15;
            this.guna2ButtonFP.Text = "Забыли пароль?";
            this.guna2ButtonFP.TextAlign = System.Windows.Forms.HorizontalAlignment.Left;
            // 
            // guna2TextBox1
            // 
            this.guna2TextBox1.Animated = true;
            this.guna2TextBox1.BorderRadius = 5;
            this.guna2TextBox1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox1.DefaultText = "";
            this.guna2TextBox1.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox1.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox1.DisabledState.Parent = this.guna2TextBox1;
            this.guna2TextBox1.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox1.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox1.FocusedState.Parent = this.guna2TextBox1;
            this.guna2TextBox1.Font = new System.Drawing.Font("Segoe UI", 20.25F);
            this.guna2TextBox1.ForeColor = System.Drawing.Color.Black;
            this.guna2TextBox1.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox1.HoverState.Parent = this.guna2TextBox1;
            this.guna2TextBox1.Location = new System.Drawing.Point(128, 50);
            this.guna2TextBox1.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.guna2TextBox1.MaximumSize = new System.Drawing.Size(330, 53);
            this.guna2TextBox1.MinimumSize = new System.Drawing.Size(330, 53);
            this.guna2TextBox1.Name = "guna2TextBox1";
            this.guna2TextBox1.PasswordChar = '\0';
            this.guna2TextBox1.PlaceholderText = "example@mail.ru";
            this.guna2TextBox1.SelectedText = "";
            this.guna2TextBox1.ShadowDecoration.Parent = this.guna2TextBox1;
            this.guna2TextBox1.Size = new System.Drawing.Size(330, 53);
            this.guna2TextBox1.TabIndex = 1;
            this.guna2TextBox1.TextOffset = new System.Drawing.Point(0, -3);
            // 
            // guna2PanelReg
            // 
            this.guna2PanelReg.Controls.Add(this.guna2TextBox5);
            this.guna2PanelReg.Controls.Add(this.guna2TextBox4);
            this.guna2PanelReg.Controls.Add(this.guna2TextBox3);
            this.guna2PanelReg.Controls.Add(this.guna2Button4);
            this.guna2PanelReg.Controls.Add(this.guna2HtmlLabel6);
            this.guna2PanelReg.Controls.Add(this.guna2HtmlLabel4);
            this.guna2PanelReg.Controls.Add(this.guna2HtmlLabel3);
            this.guna2PanelReg.Location = new System.Drawing.Point(13, 56);
            this.guna2PanelReg.Name = "guna2PanelReg";
            this.guna2PanelReg.ShadowDecoration.Parent = this.guna2PanelReg;
            this.guna2PanelReg.Size = new System.Drawing.Size(485, 362);
            this.guna2PanelReg.TabIndex = 15;
            // 
            // guna2TextBox5
            // 
            this.guna2TextBox5.BorderRadius = 5;
            this.guna2TextBox5.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox5.DefaultText = "";
            this.guna2TextBox5.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox5.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox5.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox5.DisabledState.Parent = this.guna2TextBox5;
            this.guna2TextBox5.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox5.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox5.FocusedState.Parent = this.guna2TextBox5;
            this.guna2TextBox5.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2TextBox5.ForeColor = System.Drawing.Color.Black;
            this.guna2TextBox5.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox5.HoverState.Parent = this.guna2TextBox5;
            this.guna2TextBox5.Location = new System.Drawing.Point(128, 111);
            this.guna2TextBox5.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.guna2TextBox5.Name = "guna2TextBox5";
            this.guna2TextBox5.PasswordChar = '\0';
            this.guna2TextBox5.PlaceholderText = "*******";
            this.guna2TextBox5.SelectedText = "";
            this.guna2TextBox5.ShadowDecoration.Parent = this.guna2TextBox5;
            this.guna2TextBox5.Size = new System.Drawing.Size(330, 53);
            this.guna2TextBox5.TabIndex = 2;
            this.guna2TextBox5.TextOffset = new System.Drawing.Point(0, 3);
            // 
            // guna2TextBox4
            // 
            this.guna2TextBox4.BorderRadius = 5;
            this.guna2TextBox4.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox4.DefaultText = "";
            this.guna2TextBox4.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox4.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox4.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox4.DisabledState.Parent = this.guna2TextBox4;
            this.guna2TextBox4.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox4.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox4.FocusedState.Parent = this.guna2TextBox4;
            this.guna2TextBox4.Font = new System.Drawing.Font("Segoe UI", 20.25F);
            this.guna2TextBox4.ForeColor = System.Drawing.Color.Black;
            this.guna2TextBox4.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox4.HoverState.Parent = this.guna2TextBox4;
            this.guna2TextBox4.Location = new System.Drawing.Point(128, 172);
            this.guna2TextBox4.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.guna2TextBox4.Name = "guna2TextBox4";
            this.guna2TextBox4.PasswordChar = '\0';
            this.guna2TextBox4.PlaceholderText = "*******";
            this.guna2TextBox4.SelectedText = "";
            this.guna2TextBox4.ShadowDecoration.Parent = this.guna2TextBox4;
            this.guna2TextBox4.Size = new System.Drawing.Size(330, 53);
            this.guna2TextBox4.TabIndex = 3;
            this.guna2TextBox4.TextOffset = new System.Drawing.Point(0, 3);
            // 
            // guna2TextBox3
            // 
            this.guna2TextBox3.BorderRadius = 5;
            this.guna2TextBox3.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.guna2TextBox3.DefaultText = "";
            this.guna2TextBox3.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.guna2TextBox3.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.guna2TextBox3.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox3.DisabledState.Parent = this.guna2TextBox3;
            this.guna2TextBox3.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.guna2TextBox3.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox3.FocusedState.Parent = this.guna2TextBox3;
            this.guna2TextBox3.Font = new System.Drawing.Font("Segoe UI", 20.25F);
            this.guna2TextBox3.ForeColor = System.Drawing.Color.Black;
            this.guna2TextBox3.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.guna2TextBox3.HoverState.Parent = this.guna2TextBox3;
            this.guna2TextBox3.Location = new System.Drawing.Point(128, 50);
            this.guna2TextBox3.Margin = new System.Windows.Forms.Padding(6, 7, 6, 7);
            this.guna2TextBox3.Name = "guna2TextBox3";
            this.guna2TextBox3.PasswordChar = '\0';
            this.guna2TextBox3.PlaceholderText = "example@mail.ru";
            this.guna2TextBox3.SelectedText = "";
            this.guna2TextBox3.ShadowDecoration.Parent = this.guna2TextBox3;
            this.guna2TextBox3.Size = new System.Drawing.Size(330, 53);
            this.guna2TextBox3.TabIndex = 1;
            this.guna2TextBox3.TextOffset = new System.Drawing.Point(0, -3);
            // 
            // guna2Button4
            // 
            this.guna2Button4.BorderRadius = 5;
            this.guna2Button4.CheckedState.Parent = this.guna2Button4;
            this.guna2Button4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.guna2Button4.CustomImages.Parent = this.guna2Button4;
            this.guna2Button4.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(21)))), ((int)(((byte)(24)))));
            this.guna2Button4.Font = new System.Drawing.Font("Segoe UI", 36F);
            this.guna2Button4.ForeColor = System.Drawing.Color.White;
            this.guna2Button4.HoverState.Parent = this.guna2Button4;
            this.guna2Button4.Location = new System.Drawing.Point(16, 257);
            this.guna2Button4.Name = "guna2Button4";
            this.guna2Button4.ShadowDecoration.Parent = this.guna2Button4;
            this.guna2Button4.Size = new System.Drawing.Size(453, 77);
            this.guna2Button4.TabIndex = 3;
            this.guna2Button4.Text = "Регистрация";
            this.guna2Button4.TextOffset = new System.Drawing.Point(0, -3);
            // 
            // guna2HtmlLabel6
            // 
            this.guna2HtmlLabel6.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel6.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel6.Location = new System.Drawing.Point(16, 175);
            this.guna2HtmlLabel6.Name = "guna2HtmlLabel6";
            this.guna2HtmlLabel6.Size = new System.Drawing.Size(96, 39);
            this.guna2HtmlLabel6.TabIndex = 2;
            this.guna2HtmlLabel6.Text = "Пароль";
            // 
            // guna2HtmlLabel4
            // 
            this.guna2HtmlLabel4.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel4.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel4.Location = new System.Drawing.Point(16, 115);
            this.guna2HtmlLabel4.Name = "guna2HtmlLabel4";
            this.guna2HtmlLabel4.Size = new System.Drawing.Size(96, 39);
            this.guna2HtmlLabel4.TabIndex = 1;
            this.guna2HtmlLabel4.Text = "Пароль";
            // 
            // guna2HtmlLabel3
            // 
            this.guna2HtmlLabel3.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel3.Font = new System.Drawing.Font("Segoe UI", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel3.Location = new System.Drawing.Point(16, 55);
            this.guna2HtmlLabel3.Name = "guna2HtmlLabel3";
            this.guna2HtmlLabel3.Size = new System.Drawing.Size(79, 39);
            this.guna2HtmlLabel3.TabIndex = 0;
            this.guna2HtmlLabel3.Text = "Логин";
            // 
            // Fm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(244)))), ((int)(((byte)(245)))), ((int)(((byte)(255)))));
            this.ClientSize = new System.Drawing.Size(510, 430);
            this.ControlBox = false;
            this.Controls.Add(this.guna2PanelForm);
            this.ForeColor = System.Drawing.SystemColors.ControlText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.MaximumSize = new System.Drawing.Size(510, 430);
            this.MinimumSize = new System.Drawing.Size(510, 430);
            this.Name = "Fm";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Анализ напряженно-деформированного состояния балок";
            this.guna2PanelForm.ResumeLayout(false);
            this.guna2PanelEnt.ResumeLayout(false);
            this.guna2PanelEnt.PerformLayout();
            this.guna2PanelReg.ResumeLayout(false);
            this.guna2PanelReg.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox2;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel1;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel2;
        private Guna.UI2.WinForms.Guna2Button guna2Button1;
        private Guna.UI2.WinForms.Guna2Panel guna2PanelForm;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox1;
        private Guna.UI2.WinForms.Guna2Button guna2ButtonEnt;
        private Guna.UI2.WinForms.Guna2Button guna2ButtonReg;
        private Guna.UI2.WinForms.Guna2Panel guna2PanelEnt;
        private Guna.UI2.WinForms.Guna2Panel guna2PanelReg;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox5;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox4;
        private Guna.UI2.WinForms.Guna2TextBox guna2TextBox3;
        private Guna.UI2.WinForms.Guna2Button guna2Button4;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel6;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel4;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel3;
        private

        Guna.UI2.WinForms.Guna2Button guna2ButtonFP;
    }
}