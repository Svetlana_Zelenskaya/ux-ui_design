﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Guna;
using System.Windows.Forms;

namespace program
{
    public partial class Fm : Form
    {
        string login = "login@mail.ru";
        string password = "0000000";

        public Fm()
        {
            InitializeComponent();
            guna2PanelEnt.Parent = guna2PanelReg.Parent;
            guna2ButtonReg.Checked = true;
            guna2ButtonEnt.Checked = true;
            guna2ButtonFP.Checked = true;
            guna2ButtonEnt.Font = new Font("Segoe UI", 24, FontStyle.Underline);

            guna2TextBox4.TextChanged += Guna2TextBox4_TextChanged;
            guna2TextBox2.KeyDown += Guna2TextBox2_KeyDown;

            guna2Button1.Click += Guna2Button1_Click;
        }

        private void Guna2Button1_Click(object sender, EventArgs e)
        {
            if (guna2TextBox1.Text != login)
                guna2TextBox1.BorderColor = Color.Red;
            else
                guna2TextBox1.BorderColor = Color.FromArgb(213, 218, 223);
            if (guna2TextBox2.Text != password)
                guna2TextBox2.BorderColor = Color.Red;
            else
                guna2TextBox2.BorderColor = Color.FromArgb(213, 218, 223);
            if (guna2TextBox1.Text == login && guna2TextBox2.Text == password)
            {
                Main main = new Main();
                User.form = main;
                main.ShowDialog();
                this.Dispose();
            }
        }

        private void Guna2TextBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                Guna2Button1_Click(sender, e);
            }
        }

        private void Guna2TextBox4_TextChanged(object sender, EventArgs e)
        {
            if (guna2TextBox4.Text != guna2TextBox5.Text)
            {
                guna2TextBox4.ForeColor = Color.Red;
            }
            else
                guna2TextBox4.ForeColor = Color.Black;
        }

        private void guna2ButtonReg_Click(object sender, EventArgs e)
        {
            guna2ButtonReg.Font = new Font("Segoe UI", 24, FontStyle.Underline);
            guna2ButtonEnt.Font = new Font("Segoe UI", 24, FontStyle.Regular);
            guna2PanelReg.Visible = true;
            guna2PanelEnt.Visible = false;
        }

        private void guna2ButtonEnt_Click(object sender, EventArgs e)
        {
            guna2ButtonEnt.Font = new Font("Segoe UI", 24, FontStyle.Underline);
            guna2ButtonReg.Font = new Font("Segoe UI", 24, FontStyle.Regular);
            guna2PanelEnt.Visible = true;
            guna2PanelReg.Visible = false;
        }

        
    }
}
