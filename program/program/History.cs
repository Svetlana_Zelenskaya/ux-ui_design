﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace program
{
    public partial class History : UserControl
    {
        public History()
        {
            InitializeComponent();

            guna2ImageButton3.Click += Guna2ImageButton3_Click;
        }

        private void Guna2ImageButton3_Click(object sender, EventArgs e)
        {
            guna2ImageButton3.Checked = !guna2ImageButton3.Checked;
        }
    }
}
