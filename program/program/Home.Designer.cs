﻿
namespace program
{
    partial class Home
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.guna2PanelMain = new Guna.UI2.WinForms.Guna2Panel();
            this.guna2HtmlLabel16 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.pictureBox1 = new Guna.UI2.WinForms.Guna2PictureBox();
            this.guna2Panel2 = new Guna.UI2.WinForms.Guna2Panel();
            this.guna2Panel5 = new Guna.UI2.WinForms.Guna2Panel();
            this.buExport = new Guna.UI2.WinForms.Guna2Button();
            this.guna2Panel4 = new Guna.UI2.WinForms.Guna2Panel();
            this.textBox_z1 = new Guna.UI2.WinForms.Guna2TextBox();
            this.guna2Separator2 = new Guna.UI2.WinForms.Guna2Separator();
            this.guna2HtmlLabel11 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.labelMx = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.labelQy = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.trackBar_z1 = new Guna.UI2.WinForms.Guna2TrackBar();
            this.guna2HtmlLabel12 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel13 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2Panel3 = new Guna.UI2.WinForms.Guna2Panel();
            this.textBox_c = new Guna.UI2.WinForms.Guna2TextBox();
            this.textBox_a = new Guna.UI2.WinForms.Guna2TextBox();
            this.buReset = new Guna.UI2.WinForms.Guna2Button();
            this.guna2HtmlLabel5 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel4 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.label_b = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel10 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel8 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel2 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel3 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel9 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel7 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2HtmlLabel1 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.comboBox_q = new Guna.UI2.WinForms.Guna2ComboBox();
            this.comboBox_l = new Guna.UI2.WinForms.Guna2ComboBox();
            this.trackBar_a = new Guna.UI2.WinForms.Guna2TrackBar();
            this.trackBar_c = new Guna.UI2.WinForms.Guna2TrackBar();
            this.guna2Separator1 = new Guna.UI2.WinForms.Guna2Separator();
            this.guna2HtmlLabel17 = new Guna.UI2.WinForms.Guna2HtmlLabel();
            this.guna2PanelMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.guna2Panel2.SuspendLayout();
            this.guna2Panel5.SuspendLayout();
            this.guna2Panel4.SuspendLayout();
            this.guna2Panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // guna2PanelMain
            // 
            this.guna2PanelMain.Controls.Add(this.guna2HtmlLabel16);
            this.guna2PanelMain.Controls.Add(this.pictureBox1);
            this.guna2PanelMain.Controls.Add(this.guna2Panel2);
            this.guna2PanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.guna2PanelMain.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.guna2PanelMain.Location = new System.Drawing.Point(0, 0);
            this.guna2PanelMain.Name = "guna2PanelMain";
            this.guna2PanelMain.ShadowDecoration.Parent = this.guna2PanelMain;
            this.guna2PanelMain.Size = new System.Drawing.Size(1366, 645);
            this.guna2PanelMain.TabIndex = 2;
            // 
            // guna2HtmlLabel16
            // 
            this.guna2HtmlLabel16.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel16.Font = new System.Drawing.Font("Segoe UI Semibold", 39.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel16.Location = new System.Drawing.Point(660, 8);
            this.guna2HtmlLabel16.Name = "guna2HtmlLabel16";
            this.guna2HtmlLabel16.Size = new System.Drawing.Size(418, 73);
            this.guna2HtmlLabel16.TabIndex = 2;
            this.guna2HtmlLabel16.Text = "Расчетная схема";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.pictureBox1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(249)))), ((int)(((byte)(251)))), ((int)(((byte)(255)))));
            this.pictureBox1.Location = new System.Drawing.Point(612, 70);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.ShadowDecoration.Parent = this.pictureBox1;
            this.pictureBox1.Size = new System.Drawing.Size(500, 551);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 1;
            this.pictureBox1.TabStop = false;
            // 
            // guna2Panel2
            // 
            this.guna2Panel2.Controls.Add(this.guna2Panel5);
            this.guna2Panel2.Controls.Add(this.guna2Panel4);
            this.guna2Panel2.Controls.Add(this.guna2Panel3);
            this.guna2Panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.guna2Panel2.Location = new System.Drawing.Point(0, 0);
            this.guna2Panel2.Name = "guna2Panel2";
            this.guna2Panel2.ShadowDecoration.Parent = this.guna2Panel2;
            this.guna2Panel2.Size = new System.Drawing.Size(355, 645);
            this.guna2Panel2.TabIndex = 0;
            // 
            // guna2Panel5
            // 
            this.guna2Panel5.Controls.Add(this.buExport);
            this.guna2Panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.guna2Panel5.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(240)))), ((int)(((byte)(253)))));
            this.guna2Panel5.Location = new System.Drawing.Point(0, 577);
            this.guna2Panel5.Name = "guna2Panel5";
            this.guna2Panel5.ShadowDecoration.Parent = this.guna2Panel5;
            this.guna2Panel5.Size = new System.Drawing.Size(355, 68);
            this.guna2Panel5.TabIndex = 2;
            // 
            // buExport
            // 
            this.buExport.BackColor = System.Drawing.Color.Transparent;
            this.buExport.BorderRadius = 5;
            this.buExport.BorderThickness = 1;
            this.buExport.CheckedState.Parent = this.buExport;
            this.buExport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buExport.CustomBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.buExport.CustomImages.Parent = this.buExport;
            this.buExport.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.buExport.Font = new System.Drawing.Font("Segoe UI", 20F);
            this.buExport.ForeColor = System.Drawing.Color.Black;
            this.buExport.HoverState.Parent = this.buExport;
            this.buExport.Location = new System.Drawing.Point(89, 15);
            this.buExport.Name = "buExport";
            this.buExport.ShadowDecoration.Color = System.Drawing.Color.Gray;
            this.buExport.ShadowDecoration.Parent = this.buExport;
            this.buExport.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 4, 4);
            this.buExport.Size = new System.Drawing.Size(164, 40);
            this.buExport.TabIndex = 5;
            this.buExport.Text = "Экспорт";
            this.buExport.TextOffset = new System.Drawing.Point(0, -3);
            // 
            // guna2Panel4
            // 
            this.guna2Panel4.Controls.Add(this.textBox_z1);
            this.guna2Panel4.Controls.Add(this.guna2Separator2);
            this.guna2Panel4.Controls.Add(this.guna2HtmlLabel11);
            this.guna2Panel4.Controls.Add(this.labelMx);
            this.guna2Panel4.Controls.Add(this.labelQy);
            this.guna2Panel4.Controls.Add(this.trackBar_z1);
            this.guna2Panel4.Controls.Add(this.guna2HtmlLabel12);
            this.guna2Panel4.Controls.Add(this.guna2HtmlLabel13);
            this.guna2Panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.guna2Panel4.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(240)))), ((int)(((byte)(253)))));
            this.guna2Panel4.Location = new System.Drawing.Point(0, 345);
            this.guna2Panel4.Name = "guna2Panel4";
            this.guna2Panel4.ShadowDecoration.Parent = this.guna2Panel4;
            this.guna2Panel4.Size = new System.Drawing.Size(355, 232);
            this.guna2Panel4.TabIndex = 1;
            // 
            // textBox_z1
            // 
            this.textBox_z1.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox_z1.DefaultText = "";
            this.textBox_z1.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.textBox_z1.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.textBox_z1.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.textBox_z1.DisabledState.Parent = this.textBox_z1;
            this.textBox_z1.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.textBox_z1.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.textBox_z1.FocusedState.Parent = this.textBox_z1;
            this.textBox_z1.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.textBox_z1.ForeColor = System.Drawing.Color.Black;
            this.textBox_z1.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.textBox_z1.HoverState.Parent = this.textBox_z1;
            this.textBox_z1.Location = new System.Drawing.Point(130, 61);
            this.textBox_z1.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.textBox_z1.Name = "textBox_z1";
            this.textBox_z1.PasswordChar = '\0';
            this.textBox_z1.PlaceholderText = "";
            this.textBox_z1.SelectedText = "";
            this.textBox_z1.ShadowDecoration.Parent = this.textBox_z1;
            this.textBox_z1.Size = new System.Drawing.Size(80, 31);
            this.textBox_z1.TabIndex = 6;
            // 
            // guna2Separator2
            // 
            this.guna2Separator2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(240)))), ((int)(((byte)(253)))));
            this.guna2Separator2.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.guna2Separator2.Location = new System.Drawing.Point(0, 222);
            this.guna2Separator2.Name = "guna2Separator2";
            this.guna2Separator2.Size = new System.Drawing.Size(355, 10);
            this.guna2Separator2.TabIndex = 0;
            // 
            // guna2HtmlLabel11
            // 
            this.guna2HtmlLabel11.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel11.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel11.Location = new System.Drawing.Point(102, 6);
            this.guna2HtmlLabel11.Name = "guna2HtmlLabel11";
            this.guna2HtmlLabel11.Size = new System.Drawing.Size(142, 47);
            this.guna2HtmlLabel11.TabIndex = 4;
            this.guna2HtmlLabel11.Text = "Значения";
            // 
            // labelMx
            // 
            this.labelMx.BackColor = System.Drawing.Color.Transparent;
            this.labelMx.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelMx.Location = new System.Drawing.Point(92, 177);
            this.labelMx.Name = "labelMx";
            this.labelMx.Size = new System.Drawing.Size(152, 27);
            this.labelMx.TabIndex = 3;
            this.labelMx.Text = "Mx = 3750 [кН*м]";
            // 
            // labelQy
            // 
            this.labelQy.BackColor = System.Drawing.Color.Transparent;
            this.labelQy.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.labelQy.Location = new System.Drawing.Point(107, 144);
            this.labelQy.Name = "labelQy";
            this.labelQy.Size = new System.Drawing.Size(122, 27);
            this.labelQy.TabIndex = 3;
            this.labelQy.Text = "Qy = 0.00 [kH]";
            // 
            // trackBar_z1
            // 
            this.trackBar_z1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(240)))), ((int)(((byte)(253)))));
            this.trackBar_z1.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(200)))), ((int)(((byte)(207)))));
            this.trackBar_z1.HoverState.Parent = this.trackBar_z1;
            this.trackBar_z1.IndicateFocus = false;
            this.trackBar_z1.Location = new System.Drawing.Point(25, 94);
            this.trackBar_z1.Name = "trackBar_z1";
            this.trackBar_z1.Size = new System.Drawing.Size(300, 40);
            this.trackBar_z1.TabIndex = 1;
            this.trackBar_z1.ThumbColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(21)))), ((int)(((byte)(24)))));
            // 
            // guna2HtmlLabel12
            // 
            this.guna2HtmlLabel12.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel12.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel12.Location = new System.Drawing.Point(212, 61);
            this.guna2HtmlLabel12.Name = "guna2HtmlLabel12";
            this.guna2HtmlLabel12.Size = new System.Drawing.Size(41, 27);
            this.guna2HtmlLabel12.TabIndex = 3;
            this.guna2HtmlLabel12.Text = "[мм]";
            // 
            // guna2HtmlLabel13
            // 
            this.guna2HtmlLabel13.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel13.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel13.Location = new System.Drawing.Point(89, 61);
            this.guna2HtmlLabel13.Name = "guna2HtmlLabel13";
            this.guna2HtmlLabel13.Size = new System.Drawing.Size(40, 27);
            this.guna2HtmlLabel13.TabIndex = 3;
            this.guna2HtmlLabel13.Text = "z1 =";
            // 
            // guna2Panel3
            // 
            this.guna2Panel3.Controls.Add(this.textBox_c);
            this.guna2Panel3.Controls.Add(this.textBox_a);
            this.guna2Panel3.Controls.Add(this.buReset);
            this.guna2Panel3.Controls.Add(this.guna2HtmlLabel5);
            this.guna2Panel3.Controls.Add(this.guna2HtmlLabel4);
            this.guna2Panel3.Controls.Add(this.label_b);
            this.guna2Panel3.Controls.Add(this.guna2HtmlLabel10);
            this.guna2Panel3.Controls.Add(this.guna2HtmlLabel8);
            this.guna2Panel3.Controls.Add(this.guna2HtmlLabel2);
            this.guna2Panel3.Controls.Add(this.guna2HtmlLabel3);
            this.guna2Panel3.Controls.Add(this.guna2HtmlLabel9);
            this.guna2Panel3.Controls.Add(this.guna2HtmlLabel7);
            this.guna2Panel3.Controls.Add(this.guna2HtmlLabel1);
            this.guna2Panel3.Controls.Add(this.comboBox_q);
            this.guna2Panel3.Controls.Add(this.comboBox_l);
            this.guna2Panel3.Controls.Add(this.trackBar_a);
            this.guna2Panel3.Controls.Add(this.trackBar_c);
            this.guna2Panel3.Controls.Add(this.guna2Separator1);
            this.guna2Panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.guna2Panel3.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(240)))), ((int)(((byte)(253)))));
            this.guna2Panel3.Location = new System.Drawing.Point(0, 0);
            this.guna2Panel3.Name = "guna2Panel3";
            this.guna2Panel3.ShadowDecoration.Parent = this.guna2Panel3;
            this.guna2Panel3.Size = new System.Drawing.Size(355, 345);
            this.guna2Panel3.TabIndex = 0;
            // 
            // textBox_c
            // 
            this.textBox_c.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox_c.DefaultText = "";
            this.textBox_c.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.textBox_c.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.textBox_c.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.textBox_c.DisabledState.Parent = this.textBox_c;
            this.textBox_c.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.textBox_c.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.textBox_c.FocusedState.Parent = this.textBox_c;
            this.textBox_c.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.textBox_c.ForeColor = System.Drawing.Color.Black;
            this.textBox_c.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.textBox_c.HoverState.Parent = this.textBox_c;
            this.textBox_c.Location = new System.Drawing.Point(220, 73);
            this.textBox_c.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.textBox_c.Name = "textBox_c";
            this.textBox_c.PasswordChar = '\0';
            this.textBox_c.PlaceholderText = "";
            this.textBox_c.SelectedText = "";
            this.textBox_c.ShadowDecoration.Parent = this.textBox_c;
            this.textBox_c.Size = new System.Drawing.Size(80, 31);
            this.textBox_c.TabIndex = 6;
            // 
            // textBox_a
            // 
            this.textBox_a.Cursor = System.Windows.Forms.Cursors.IBeam;
            this.textBox_a.DefaultText = "";
            this.textBox_a.DisabledState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(208)))), ((int)(((byte)(208)))), ((int)(((byte)(208)))));
            this.textBox_a.DisabledState.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(226)))), ((int)(((byte)(226)))), ((int)(((byte)(226)))));
            this.textBox_a.DisabledState.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.textBox_a.DisabledState.Parent = this.textBox_a;
            this.textBox_a.DisabledState.PlaceholderForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(138)))), ((int)(((byte)(138)))), ((int)(((byte)(138)))));
            this.textBox_a.FocusedState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.textBox_a.FocusedState.Parent = this.textBox_a;
            this.textBox_a.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.textBox_a.ForeColor = System.Drawing.Color.Black;
            this.textBox_a.HoverState.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(94)))), ((int)(((byte)(148)))), ((int)(((byte)(255)))));
            this.textBox_a.HoverState.Parent = this.textBox_a;
            this.textBox_a.Location = new System.Drawing.Point(46, 73);
            this.textBox_a.Margin = new System.Windows.Forms.Padding(5, 5, 5, 5);
            this.textBox_a.Name = "textBox_a";
            this.textBox_a.PasswordChar = '\0';
            this.textBox_a.PlaceholderText = "";
            this.textBox_a.SelectedText = "";
            this.textBox_a.ShadowDecoration.Parent = this.textBox_a;
            this.textBox_a.Size = new System.Drawing.Size(80, 31);
            this.textBox_a.TabIndex = 6;
            // 
            // buReset
            // 
            this.buReset.BackColor = System.Drawing.Color.Transparent;
            this.buReset.BorderRadius = 5;
            this.buReset.CheckedState.Parent = this.buReset;
            this.buReset.Cursor = System.Windows.Forms.Cursors.Hand;
            this.buReset.CustomImages.Parent = this.buReset;
            this.buReset.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(238)))), ((int)(((byte)(34)))), ((int)(((byte)(34)))));
            this.buReset.Font = new System.Drawing.Font("Segoe UI", 20F);
            this.buReset.ForeColor = System.Drawing.Color.White;
            this.buReset.HoverState.Parent = this.buReset;
            this.buReset.Location = new System.Drawing.Point(86, 282);
            this.buReset.Name = "buReset";
            this.buReset.ShadowDecoration.Color = System.Drawing.Color.Gray;
            this.buReset.ShadowDecoration.Enabled = true;
            this.buReset.ShadowDecoration.Parent = this.buReset;
            this.buReset.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 4, 4);
            this.buReset.Size = new System.Drawing.Size(164, 40);
            this.buReset.TabIndex = 5;
            this.buReset.Text = "Сброс";
            this.buReset.TextOffset = new System.Drawing.Point(0, -3);
            // 
            // guna2HtmlLabel5
            // 
            this.guna2HtmlLabel5.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel5.Font = new System.Drawing.Font("Segoe UI", 24F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel5.Location = new System.Drawing.Point(92, 8);
            this.guna2HtmlLabel5.Name = "guna2HtmlLabel5";
            this.guna2HtmlLabel5.Size = new System.Drawing.Size(171, 47);
            this.guna2HtmlLabel5.TabIndex = 4;
            this.guna2HtmlLabel5.Text = "Параметры";
            // 
            // guna2HtmlLabel4
            // 
            this.guna2HtmlLabel4.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel4.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel4.Location = new System.Drawing.Point(189, 75);
            this.guna2HtmlLabel4.Name = "guna2HtmlLabel4";
            this.guna2HtmlLabel4.Size = new System.Drawing.Size(30, 27);
            this.guna2HtmlLabel4.TabIndex = 3;
            this.guna2HtmlLabel4.Text = "c =";
            // 
            // label_b
            // 
            this.label_b.BackColor = System.Drawing.Color.Transparent;
            this.label_b.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label_b.Location = new System.Drawing.Point(107, 151);
            this.label_b.Name = "label_b";
            this.label_b.Size = new System.Drawing.Size(120, 27);
            this.label_b.TabIndex = 3;
            this.label_b.Text = "b = 1000 [мм]";
            // 
            // guna2HtmlLabel10
            // 
            this.guna2HtmlLabel10.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel10.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel10.Location = new System.Drawing.Point(69, 236);
            this.guna2HtmlLabel10.Name = "guna2HtmlLabel10";
            this.guna2HtmlLabel10.Size = new System.Drawing.Size(32, 27);
            this.guna2HtmlLabel10.TabIndex = 3;
            this.guna2HtmlLabel10.Text = "q =";
            // 
            // guna2HtmlLabel8
            // 
            this.guna2HtmlLabel8.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel8.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel8.Location = new System.Drawing.Point(75, 190);
            this.guna2HtmlLabel8.Name = "guna2HtmlLabel8";
            this.guna2HtmlLabel8.Size = new System.Drawing.Size(26, 27);
            this.guna2HtmlLabel8.TabIndex = 3;
            this.guna2HtmlLabel8.Text = "l =";
            // 
            // guna2HtmlLabel2
            // 
            this.guna2HtmlLabel2.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel2.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel2.Location = new System.Drawing.Point(12, 75);
            this.guna2HtmlLabel2.Name = "guna2HtmlLabel2";
            this.guna2HtmlLabel2.Size = new System.Drawing.Size(31, 27);
            this.guna2HtmlLabel2.TabIndex = 3;
            this.guna2HtmlLabel2.Text = "a =";
            // 
            // guna2HtmlLabel3
            // 
            this.guna2HtmlLabel3.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel3.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel3.Location = new System.Drawing.Point(304, 75);
            this.guna2HtmlLabel3.Name = "guna2HtmlLabel3";
            this.guna2HtmlLabel3.Size = new System.Drawing.Size(41, 27);
            this.guna2HtmlLabel3.TabIndex = 3;
            this.guna2HtmlLabel3.Text = "[мм]";
            // 
            // guna2HtmlLabel9
            // 
            this.guna2HtmlLabel9.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel9.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel9.Location = new System.Drawing.Point(208, 236);
            this.guna2HtmlLabel9.Name = "guna2HtmlLabel9";
            this.guna2HtmlLabel9.Size = new System.Drawing.Size(57, 27);
            this.guna2HtmlLabel9.TabIndex = 3;
            this.guna2HtmlLabel9.Text = "[kH/м]";
            // 
            // guna2HtmlLabel7
            // 
            this.guna2HtmlLabel7.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel7.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel7.Location = new System.Drawing.Point(208, 190);
            this.guna2HtmlLabel7.Name = "guna2HtmlLabel7";
            this.guna2HtmlLabel7.Size = new System.Drawing.Size(41, 27);
            this.guna2HtmlLabel7.TabIndex = 3;
            this.guna2HtmlLabel7.Text = "[мм]";
            // 
            // guna2HtmlLabel1
            // 
            this.guna2HtmlLabel1.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel1.Font = new System.Drawing.Font("Segoe UI", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel1.Location = new System.Drawing.Point(127, 75);
            this.guna2HtmlLabel1.Name = "guna2HtmlLabel1";
            this.guna2HtmlLabel1.Size = new System.Drawing.Size(41, 27);
            this.guna2HtmlLabel1.TabIndex = 3;
            this.guna2HtmlLabel1.Text = "[мм]";
            // 
            // comboBox_q
            // 
            this.comboBox_q.BackColor = System.Drawing.Color.Transparent;
            this.comboBox_q.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.comboBox_q.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_q.FocusedColor = System.Drawing.Color.Black;
            this.comboBox_q.FocusedState.BorderColor = System.Drawing.Color.Black;
            this.comboBox_q.FocusedState.Parent = this.comboBox_q;
            this.comboBox_q.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.comboBox_q.ForeColor = System.Drawing.Color.Black;
            this.comboBox_q.HoverState.BorderColor = System.Drawing.Color.Black;
            this.comboBox_q.HoverState.FillColor = System.Drawing.Color.Silver;
            this.comboBox_q.HoverState.Parent = this.comboBox_q;
            this.comboBox_q.ItemHeight = 25;
            this.comboBox_q.Items.AddRange(new object[] {
            "4",
            "5",
            "6"});
            this.comboBox_q.ItemsAppearance.Parent = this.comboBox_q;
            this.comboBox_q.Location = new System.Drawing.Point(107, 234);
            this.comboBox_q.Name = "comboBox_q";
            this.comboBox_q.ShadowDecoration.Color = System.Drawing.Color.Gray;
            this.comboBox_q.ShadowDecoration.Enabled = true;
            this.comboBox_q.ShadowDecoration.Parent = this.comboBox_q;
            this.comboBox_q.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 4, 4);
            this.comboBox_q.Size = new System.Drawing.Size(99, 31);
            this.comboBox_q.TabIndex = 2;
            // 
            // comboBox_l
            // 
            this.comboBox_l.BackColor = System.Drawing.Color.Transparent;
            this.comboBox_l.DrawMode = System.Windows.Forms.DrawMode.OwnerDrawFixed;
            this.comboBox_l.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBox_l.FocusedColor = System.Drawing.Color.Black;
            this.comboBox_l.FocusedState.BorderColor = System.Drawing.Color.Black;
            this.comboBox_l.FocusedState.Parent = this.comboBox_l;
            this.comboBox_l.Font = new System.Drawing.Font("Segoe UI", 14F);
            this.comboBox_l.ForeColor = System.Drawing.Color.Black;
            this.comboBox_l.HoverState.BorderColor = System.Drawing.Color.Black;
            this.comboBox_l.HoverState.FillColor = System.Drawing.Color.Silver;
            this.comboBox_l.HoverState.Parent = this.comboBox_l;
            this.comboBox_l.ItemHeight = 25;
            this.comboBox_l.Items.AddRange(new object[] {
            "2000",
            "2500",
            "3000"});
            this.comboBox_l.ItemsAppearance.Parent = this.comboBox_l;
            this.comboBox_l.Location = new System.Drawing.Point(107, 188);
            this.comboBox_l.Name = "comboBox_l";
            this.comboBox_l.ShadowDecoration.Color = System.Drawing.Color.Gray;
            this.comboBox_l.ShadowDecoration.Enabled = true;
            this.comboBox_l.ShadowDecoration.Parent = this.comboBox_l;
            this.comboBox_l.ShadowDecoration.Shadow = new System.Windows.Forms.Padding(0, 0, 4, 4);
            this.comboBox_l.Size = new System.Drawing.Size(99, 31);
            this.comboBox_l.TabIndex = 2;
            // 
            // trackBar_a
            // 
            this.trackBar_a.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(240)))), ((int)(((byte)(253)))));
            this.trackBar_a.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(200)))), ((int)(((byte)(207)))));
            this.trackBar_a.HoverState.Parent = this.trackBar_a;
            this.trackBar_a.IndicateFocus = false;
            this.trackBar_a.Location = new System.Drawing.Point(22, 109);
            this.trackBar_a.Maximum = 1500;
            this.trackBar_a.Name = "trackBar_a";
            this.trackBar_a.Size = new System.Drawing.Size(138, 40);
            this.trackBar_a.TabIndex = 1;
            this.trackBar_a.ThumbColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(21)))), ((int)(((byte)(24)))));
            this.trackBar_a.Value = 1000;
            // 
            // trackBar_c
            // 
            this.trackBar_c.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(240)))), ((int)(((byte)(253)))));
            this.trackBar_c.FillColor = System.Drawing.Color.FromArgb(((int)(((byte)(193)))), ((int)(((byte)(200)))), ((int)(((byte)(207)))));
            this.trackBar_c.HoverState.Parent = this.trackBar_c;
            this.trackBar_c.IndicateFocus = false;
            this.trackBar_c.Location = new System.Drawing.Point(198, 109);
            this.trackBar_c.Maximum = 1500;
            this.trackBar_c.Name = "trackBar_c";
            this.trackBar_c.Size = new System.Drawing.Size(138, 40);
            this.trackBar_c.TabIndex = 1;
            this.trackBar_c.ThumbColor = System.Drawing.Color.FromArgb(((int)(((byte)(21)))), ((int)(((byte)(21)))), ((int)(((byte)(24)))));
            this.trackBar_c.Value = 1000;
            // 
            // guna2Separator1
            // 
            this.guna2Separator1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(240)))), ((int)(((byte)(253)))));
            this.guna2Separator1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.guna2Separator1.Location = new System.Drawing.Point(0, 335);
            this.guna2Separator1.Name = "guna2Separator1";
            this.guna2Separator1.Size = new System.Drawing.Size(355, 10);
            this.guna2Separator1.TabIndex = 0;
            // 
            // guna2HtmlLabel17
            // 
            this.guna2HtmlLabel17.BackColor = System.Drawing.Color.Transparent;
            this.guna2HtmlLabel17.Font = new System.Drawing.Font("Segoe UI", 40F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.guna2HtmlLabel17.Location = new System.Drawing.Point(620, 15);
            this.guna2HtmlLabel17.Name = "guna2HtmlLabel17";
            this.guna2HtmlLabel17.Size = new System.Drawing.Size(202, 73);
            this.guna2HtmlLabel17.TabIndex = 4;
            this.guna2HtmlLabel17.Text = "Расчеты";
            // 
            // Home
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.guna2PanelMain);
            this.Name = "Home";
            this.Size = new System.Drawing.Size(1366, 645);
            this.guna2PanelMain.ResumeLayout(false);
            this.guna2PanelMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.guna2Panel2.ResumeLayout(false);
            this.guna2Panel5.ResumeLayout(false);
            this.guna2Panel4.ResumeLayout(false);
            this.guna2Panel4.PerformLayout();
            this.guna2Panel3.ResumeLayout(false);
            this.guna2Panel3.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private Guna.UI2.WinForms.Guna2Panel guna2PanelMain;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel16;
        private Guna.UI2.WinForms.Guna2PictureBox pictureBox1;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel2;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel5;
        private Guna.UI2.WinForms.Guna2Button buExport;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel4;
        private Guna.UI2.WinForms.Guna2Separator guna2Separator2;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel11;
        private Guna.UI2.WinForms.Guna2HtmlLabel labelMx;
        private Guna.UI2.WinForms.Guna2HtmlLabel labelQy;
        private Guna.UI2.WinForms.Guna2TrackBar trackBar_z1;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel12;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel13;
        private Guna.UI2.WinForms.Guna2Panel guna2Panel3;
        private Guna.UI2.WinForms.Guna2Button buReset;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel5;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel4;
        private Guna.UI2.WinForms.Guna2HtmlLabel label_b;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel10;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel8;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel2;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel3;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel9;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel7;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel1;
        private Guna.UI2.WinForms.Guna2ComboBox comboBox_q;
        private Guna.UI2.WinForms.Guna2ComboBox comboBox_l;
        private Guna.UI2.WinForms.Guna2TrackBar trackBar_a;
        private Guna.UI2.WinForms.Guna2TrackBar trackBar_c;
        private Guna.UI2.WinForms.Guna2Separator guna2Separator1;
        private Guna.UI2.WinForms.Guna2HtmlLabel guna2HtmlLabel17;
        private Guna.UI2.WinForms.Guna2TextBox textBox_c;
        private Guna.UI2.WinForms.Guna2TextBox textBox_a;
        private Guna.UI2.WinForms.Guna2TextBox textBox_z1;
    }
}
