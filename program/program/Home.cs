﻿using Nevron.Nov.Diagram;
using Nevron.Nov.Diagram.Export;
using Nevron.Nov.UI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Font = System.Drawing.Font;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html;
using iTextSharp.text.html.simpleparser;

namespace program
{
    public partial class Home : System.Windows.Forms.UserControl
    {
        private float a;
        private float b;
        private float z2;
        private float c;
        private float z1;
        private float l;
        private float q;
        Label label1 = new Label();
        Label label2 = new Label();
        Label label3 = new Label();
        Label label4 = new Label();
        Label label5 = new Label();
        Label label6 = new Label();
        Label label7 = new Label();
        Label label8 = new Label();
        Label label9 = new Label();
        Label label10 = new Label();
        Label label11 = new Label();
        Label label12 = new Label();
        Label label13 = new Label();
        Label label14 = new Label();
        Label label15 = new Label();
        Label label16 = new Label();
        Label label17 = new Label();
        Label label18 = new Label();
        Label label19 = new Label();
        Label label20 = new Label();
        Label label21 = new Label();
        Label label22 = new Label();
        Bitmap bitmap;

        public Home()
        {
            InitializeComponent();


            comboBox_l.SelectedIndex = 2;
            comboBox_q.SelectedIndex = 2;

            Change_l();

            comboBox_l.SelectedIndexChanged += ComboBox_l_SelectedIndexChanged;
            comboBox_q.SelectedIndexChanged += ComboBox_q_SelectedIndexChanged;

            trackBar_a.ValueChanged += Guna2TrackBar1_ValueChanged;
            trackBar_c.ValueChanged += Guna2TrackBar2_ValueChanged;
            trackBar_z1.ValueChanged += TrackBar_z1_ValueChanged;
            trackBar_z1.MouseUp += TrackBar_z1_MouseUp;
            trackBar_a.MouseUp += TrackBar_a_MouseUp;
            trackBar_c.MouseUp += TrackBar_c_MouseUp;
            textBox_a.TextChanged += TextBox_a_TextChanged;
            textBox_c.TextChanged += TextBox_c_TextChanged;
            textBox_z1.TextChanged += TextBox_z1_TextChanged;
            textBox_a.KeyDown += TextBox_a_KeyDown;
            textBox_c.KeyDown += TextBox_c_KeyDown;
            textBox_z1.KeyDown += TextBox_z1_KeyDown;

            buReset.Click += BuReset_Click;
            buExport.Click += BuExport_Click;

            bitmap = new Bitmap(pictureBox1.Width, pictureBox1.Height);

            pictureBox1.Paint += (s, e) =>
            {
                Graphics h = Graphics.FromImage(bitmap);
                DrawDiagrame(h);
                e.Graphics.DrawImage(bitmap, 0, 0);
            };
        }

        private void BuExport_Click(object sender, EventArgs e)
        {
            Document doc = new Document(PageSize.A4, 10f, 10f, 100f, 0f);
            string pdfFilePath = @"D:/";
            string fullPdfFilePath = pdfFilePath + "MyDocument.pdf";

            var fs = new FileStream(fullPdfFilePath, FileMode.Create);
            doc.NewPage();
            var writer = PdfWriter.GetInstance(doc, fs);
           
            doc.Open();

            try
            {

                MemoryStream ms = new MemoryStream();
                pictureBox1.Image = bitmap;
                pictureBox1.Image.Save(ms, System.Drawing.Imaging.ImageFormat.Jpeg);
                iTextSharp.text.Image jpg = iTextSharp.text.Image.GetInstance(ms.GetBuffer());
                jpg.ScaleToFit(399f, 440f);
                jpg.SpacingBefore = 5f;
                jpg.SpacingAfter = 5f;
                jpg.Alignment = Element.ALIGN_LEFT;
                iTextSharp.text.Font font = FontFactory.GetFont("Segoe UI", 18.0f, BaseColor.BLACK);
                doc.Add(new Paragraph("Design scheme", new iTextSharp.text.Font(font)));
                doc.Add(jpg);
                doc.Add(new Paragraph("Parameters:", new iTextSharp.text.Font(font)));
                doc.Add(new Paragraph($"a = {trackBar_a.Value} [mm]"));
                doc.Add(new Paragraph($"b = {b} [mm]"));
                doc.Add(new Paragraph($"c = {trackBar_c.Value} [mm]"));
                doc.Add(new Paragraph($"l = {comboBox_l.Text} [mm]"));
                doc.Add(new Paragraph($"q = {comboBox_q.Text} [kN/m]"));
                doc.Add(new Paragraph("Values:", new iTextSharp.text.Font(font)));
                doc.Add(new Paragraph($"{string.Format("Ra = {0:N2} [kN]", Ra)}"));
                doc.Add(new Paragraph($"{string.Format("Rb = {0:N2} [kN]", Rb)}"));
                doc.Add(new Paragraph($"Mmax = {Mmax} [kN*m]"));
                doc.Add(new Paragraph($"z1 = {z1} [mm]: {string.Format("Qy = {0:N2} [kN]", Qy)}, {string.Format("Mx = {0:N2} [kN*m]", Mx)}"));
            }
            catch (Exception ex)
            {
                var str = ex.Message;
            }
            finally
            {
                doc.Close();
            }
            if (File.Exists(fullPdfFilePath))
            {
                System.Diagnostics.Process.Start(fullPdfFilePath);
            }
        }

        private void ComboBox_q_SelectedIndexChanged(object sender, EventArgs e)
        {
            changeMxQyValue();
            pictureBox1.Refresh();
        }

        public void SetParam()
        {
            l = Convert.ToInt32(comboBox_l.Text);
            q = Convert.ToInt32(comboBox_q.Text);
            a = trackBar_a.Value;
            c = trackBar_c.Value;
            b = l - a - c;
            z1 = trackBar_z1.Value;
            z2 = l - z1;
        }

        private float coeffH, coeffW, coeffH2, coeffW2;

        public float Mmax { get; private set; }
        public float Ra { get; private set; }
        public float Rb { get; private set; }

        private float Q(float z)
        {
            return q * b * (2 * c + b) / (2 * l);
        }
        private float Q2(float z)
        {
            return q * b * ((2 * c + b) / (2 * l) - (z - a) / b);
        }
        private float Q3(float z)
        {
            return -q * b * (2 * a + b) / (2 * l);
        }

        private float M(float z)
        {
            return q * b * (2 * c + b) / (2 * l) * z;

        }
        private float M2(float z)
        {
            return q * b * b / 2 * ((2 * c + b) / l * z / b - (z - a) * (z - a) / (b * b));

        }
        private float M3(float z)
        {
            return q * b * (2 * a + b) / (2 * l) * (l - z);

        }

        public void DrawDiagrame(Graphics h)
        {
            h.Clear(pictureBox1.BackColor);

            SetParam();
            int L = (int)l;
            int A = (int)a; //Первый цикл будет идти до этой точки
            int AB = (int)(a + b);
            PointF[] Qz = new PointF[L];
            PointF[] Mz = new PointF[L];


            //первый цикл
            float z = 0;
            for (int i = 0; i < A; i++)
            {
                Qz[i].X = z;
                Qz[i].Y = Q(z);
                Mz[i].X = z;
                Mz[i].Y = M(z);

                z += 1;
            }
            //Второй цикл
            for (int i = A; i < AB; i++)
            {
                Qz[i].X = z;
                Qz[i].Y = Q2(z);
                Mz[i].X = z;
                Mz[i].Y = M2(z);

                z += 1;
            }
            //третий цикл
            for (int i = AB; i < L; i++)
            {
                Qz[i].X = z;
                Qz[i].Y = Q3(z);
                Mz[i].X = z;
                Mz[i].Y = M3(z);

                z += 1;
            }
            //Массив для рисования

            PointF[] DrawQz = new PointF[L + 2]; // + 2 для замыкания 
            PointF[] DrawMz = new PointF[L + 2]; // + 2 для замыкания 
            //Пoиск max y
            float ymax = Qz[0].Y; // Помещаем первое число
            for (int i = 1; i < L; i++)
            {
                if (ymax < Qz[i].Y)
                    ymax = Qz[i].Y;
            }
            float ymax2 = Mz[0].Y; // Помещаем первое число
            for (int i = 1; i < L; i++)
            {
                if (ymax2 < Mz[i].Y)
                    ymax2 = Mz[i].Y;
            }

            int startX = 20; // начало оси эпюры по x
            int startY = 310;// начало оси эпюры по y
            int startY2 = 490;
            int H = 60;// высота эпюры в пикселях
            int W = 450;//ширина эпюры в пикселях

            coeffW = (float)W / (float)Qz[L - 1].X;
            coeffH = (float)H / (float)ymax;
            coeffW2 = (float)W / (float)Mz[L - 1].X;
            coeffH2 = (float)H / (float)ymax2;

            for (int i = 0; i < L; i++)
            {
                DrawQz[i].X = Qz[i].X * coeffW + startX;
                DrawQz[i].Y = -Qz[i].Y * coeffH + startY + H;
                DrawMz[i].X = Mz[i].X * coeffW2 + startX;
                DrawMz[i].Y = -Mz[i].Y * coeffH2 + startY2 + H;
            }
            // точка справа на оси
            DrawQz[L].X = DrawQz[L - 1].X;
            DrawQz[L].Y = startY + H;
            DrawMz[L].X = DrawMz[L - 1].X;
            DrawMz[L].Y = startY2 + H;

            // точка слева на оси
            DrawQz[L + 1].X = DrawQz[0].X;
            DrawQz[L + 1].Y = startY + H;
            DrawMz[L + 1].X = DrawMz[0].X;
            DrawMz[L + 1].Y = startY2 + H;

            HatchBrush Brusha = new HatchBrush(HatchStyle.Vertical, Color.Black, Color.White);

            h.FillPolygon(Brusha, DrawQz);
            h.DrawLines(Pens.Black, DrawQz);
            h.FillPolygon(Brusha, DrawMz);
            h.DrawLines(Pens.Black, DrawMz);

            //оси
            Pen pen1 = new Pen(Color.Black, 1)
            {
                StartCap = LineCap.ArrowAnchor,
                EndCap = LineCap.Custom
            };
            h.DrawLine(pen1, 20, 30, 20, 600);
            h.DrawLine(pen1, pictureBox1.Width, 160, 20, 160);

            Pen pen2 = new Pen(Color.Black, 1);
            h.DrawLine(pen2, pictureBox1.Width - 30, 160, pictureBox1.Width - 30, 600);

            createLines_a_b_c_l(h, AB);
            createLines_z(h, AB);

            //q
            h.DrawLine(pen1, 20 + a * coeffW, 160, 20 + a * coeffW, 90);
            h.DrawLine(pen1, 20 + a * coeffW + 5 * b * coeffW / 8, 160, 20 + a * coeffW + 5 * b * coeffW / 8, 90);
            h.DrawLine(pen1, 20 + a * coeffW + b * coeffW / 8, 160, 20 + a * coeffW + b * coeffW / 8, 90);
            h.DrawLine(pen1, 20 + a * coeffW + 3 * b * coeffW / 8, 160, 20 + a * coeffW + 3 * b * coeffW / 8, 90);
            h.DrawLine(pen1, 20 + a * coeffW + b * coeffW / 4, 160, 20 + a * coeffW + b * coeffW / 4, 90);
            h.DrawLine(pen1, 20 + a * coeffW + 3 * b * coeffW / 4, 160, 20 + a * coeffW + 3 * b * coeffW / 4, 90);
            h.DrawLine(pen1, 20 + a * coeffW + b * coeffW / 2, 160, 20 + a * coeffW + b * coeffW / 2, 90);
            h.DrawLine(pen1, 20 + a * coeffW + 7 * b * coeffW / 8, 160, 20 + a * coeffW + 7 * b * coeffW / 8, 90);
            h.DrawLine(pen1, 20 + a * coeffW + b * coeffW, 160, 20 + a * coeffW + b * coeffW, 90);
            h.DrawLine(pen2, 20 + a * coeffW, 90, 20 + AB * coeffW, 90);

            //Ra Rb
            Pen pen4 = new Pen(Color.Red, 5)
            {
                StartCap = LineCap.ArrowAnchor,
                EndCap = LineCap.Custom
            };
            h.DrawLine(pen4, 20, 80, 20, 160);
            h.DrawLine(pen4, 20 + l * coeffW, 80, 20 + l * coeffW, 160);

            SolidBrush mySolidBrush = new SolidBrush(Color.White);
            h.FillEllipse(mySolidBrush, 15, 155, 10, 10);
            h.FillEllipse(mySolidBrush, 15 + l * coeffW, 155, 10, 10);
            h.FillEllipse(mySolidBrush, 15 + l * coeffW, 170, 10, 10);
            h.DrawEllipse(pen2, 15, 155, 10, 10);
            h.DrawEllipse(pen2, 15 + l * coeffW, 155, 10, 10);
            h.DrawEllipse(pen2, 15 + l * coeffW, 170, 10, 10);


            h.DrawLine(pen2, 20, 163, 10, 180);
            h.DrawLine(pen2, 20, 163, 30, 180);
            h.DrawLine(pen2, 5, 181, 35, 181);

            h.DrawLine(pen2, 5, 190, 10, 181);
            h.DrawLine(pen2, 15, 190, 20, 181);
            h.DrawLine(pen2, 20, 190, 25, 181);
            h.DrawLine(pen2, 25, 190, 30, 181);
            h.DrawLine(pen2, 10, 190, 15, 181);
            //right
            h.DrawLine(pen2, 5 + l * coeffW, 181, 35 + l * coeffW, 181);
            h.DrawLine(pen2, 5 + l * coeffW, 190, 10 + l * coeffW, 181);
            h.DrawLine(pen2, 15 + l * coeffW, 190, 20 + l * coeffW, 181);
            h.DrawLine(pen2, 20 + l * coeffW, 190, 25 + l * coeffW, 181);
            h.DrawLine(pen2, 25 + l * coeffW, 190, 30 + l * coeffW, 181);
            h.DrawLine(pen2, 10 + l * coeffW, 190, 15 + l * coeffW, 181);

            
            
            
            label1.Text = "w";
            h.DrawString(label1.Text, new Font("Segoe UI", 11), Brushes.Black, new PointF(25, 20));
            label2.Text = "q";
            h.DrawString(label2.Text, new Font("Segoe UI", 11), Brushes.Black, new PointF(20 + a * coeffW + b * coeffW / 2, 70));
            label3.Text = "z";
            h.DrawString(label3.Text, new Font("Segoe UI", 11), Brushes.Black, new PointF(40 + l * coeffW, 140));
            label4.Text = "A";
            h.DrawString(label4.Text, new Font("Segoe UI", 11), Brushes.Black, new PointF(0, 140));
            label5.Text = "B";
            h.DrawString(label5.Text, new Font("Segoe UI", 11), Brushes.Black, new PointF(5 + a * coeffW, 140));
            label6.Text = "C";
            h.DrawString(label6.Text, new Font("Segoe UI", 11), Brushes.Black, new PointF(20 + AB * coeffW, 140));
            label7.Text = "D";
            h.DrawString(label7.Text, new Font("Segoe UI", 11), Brushes.Black, new PointF(25 + l * coeffW, 140));
            label8.Text = "a";
            h.DrawString(label8.Text, new Font("Segoe UI", 11), Brushes.Black, new PointF(20 + a * coeffW / 2, 190));
            label9.Text = "b";
            h.DrawString(label9.Text, new Font("Segoe UI", 11), Brushes.Black, new PointF(20 + a * coeffW + b * coeffW / 2, 190));
            label10.Text = "c";
            h.DrawString(label10.Text, new Font("Segoe UI", 11), Brushes.Black, new PointF(20 + AB * coeffW + c * coeffW / 2, 190));
            label11.Text = "l";
            h.DrawString(label11.Text, new Font("Segoe UI", 11), Brushes.Black, new PointF(20 + a * coeffW + b * coeffW / 2, 220));
            label12.Text = "Ra";
            h.DrawString(label12.Text, new Font("Segoe UI", 11), Brushes.Black, new PointF(25, 65));
            label13.Text = "Rd";
            h.DrawString(label13.Text, new Font("Segoe UI", 11), Brushes.Black, new PointF(l * coeffW - 5, 65));
            label14.Text = "z1";
            h.DrawString(label14.Text, new Font("Segoe UI", 11), Brushes.Black, new PointF((40 + z1 * coeffW)/2, 250));
            label15.Text = "0";
            h.DrawString(label15.Text, new Font("Segoe UI", 11), Brushes.Black, new PointF(5, 360));

            //label16.Text = "qb(b+2c)/(2l)";
            Ra = (int)q * b * (b + 2 * c) / (2 * l) / 1000;
            label16.Text = String.Format("Ra = {0:0.00}", Ra);
            h.DrawString(label16.Text, new Font("Segoe UI", 11), Brushes.Black, new PointF(25, 285));

            //label17.Text = "qb(2a+b)/(2l)";
            Rb = (int)q * b * (b + 2 * a) / (2 * l) / 1000;
            label17.Text = String.Format("Rb = {0:0.00}", Rb);
            h.DrawString(label17.Text, new Font("Segoe UI", 11), Brushes.Black, new PointF(l * coeffW - 50, (-Qz[L-1].Y * coeffH + startY + H + 5)));
            label18.Text = "Qy";
            h.DrawString(label18.Text, new Font("Segoe UI", 11), Brushes.Black, new PointF(l * coeffW + 20, 360));
            label19.Text = "Mx";
            h.DrawString(label19.Text, new Font("Segoe UI", 11), Brushes.Black, new PointF(l * coeffW + 20, 535));
            label20.Text = "0";
            h.DrawString(label20.Text, new Font("Segoe UI", 11), Brushes.Black, new PointF(5, 535));

            Mmax = (int)q * b * (b + 2 * c) / (2 * l) * (a + b * (2 * c + b) / (4 * l)) / 1000;
            //label22.Text = "qb(2c+b)/2l*[a+b(2c+b)/4l]";
            label22.Text = String.Format("Mmax = {0:0.00}", Mmax);
            h.DrawString(label22.Text, new Font("Segoe UI", 11), Brushes.Black, new PointF(a * coeffW + b * coeffW / 2 - 25, 470));
        
        }

        private void createLines_z(Graphics h, int AB)
        {
            Pen pen1 = new Pen(Color.FromArgb(249, 251, 255), 4);
            h.DrawLine(pen1, 20, 270, 19 + l * coeffW, 270);//z1            

            Pen pen3 = new Pen(Color.Black, 1)
            {
                StartCap = LineCap.ArrowAnchor,
                EndCap = LineCap.ArrowAnchor
            };
            
            h.DrawLine(pen3, 20, 270, 20 + z1 * coeffW, 270);//z1             
        }

        private void createLines_a_b_c_l(Graphics h, int AB)
        {
            Pen pen1 = new Pen(SystemColors.Control, 4);
            h.DrawLine(pen1, 20, 210, 20 + a * coeffW, 210);//a
            h.DrawLine(pen1, 20 + a * coeffW, 210, 20 + AB * coeffW, 210);//b
            h.DrawLine(pen1, 20 + AB * coeffW, 210, 20 + l * coeffW, 210);//c
            h.DrawLine(pen1, 20, 240, 20 + l * coeffW, 240);//l

            Pen pen3 = new Pen(Color.Black, 1)
            {
                StartCap = LineCap.ArrowAnchor,
                EndCap = LineCap.ArrowAnchor
            };
            h.DrawLine(pen3, 20, 210, 20 + a * coeffW, 210);//a
            h.DrawLine(pen3, 20 + a * coeffW, 210, 20 + AB * coeffW, 210);//b
            h.DrawLine(pen3, 20 + AB * coeffW, 210, 20 + l * coeffW, 210);//c
            h.DrawLine(pen3, 20, 240, 20 + l * coeffW, 240);//l
        }

        private void Change_l()
        {
            trackBar_a.Maximum = Convert.ToInt32(comboBox_l.Text) / 2;
            trackBar_c.Maximum = Convert.ToInt32(comboBox_l.Text) - trackBar_a.Maximum;
            trackBar_a.Value = Convert.ToInt32((trackBar_a.Maximum + trackBar_c.Maximum) / 3);
            trackBar_c.Value = trackBar_a.Value;
            trackBar_z1.Maximum = trackBar_a.Maximum + trackBar_c.Maximum;
            trackBar_z1.Value = Convert.ToInt32(trackBar_z1.Maximum / 2);

            textBox_z1.Text = trackBar_z1.Value.ToString();
            textBox_c.Text = trackBar_c.Value.ToString();
            textBox_a.Text = trackBar_a.Value.ToString();

            b = trackBar_a.Maximum - trackBar_a.Value - trackBar_a.Value + trackBar_c.Maximum;
            label_b.Text = String.Format("b = {0} [мм]", (int)b);

            pictureBox1.Refresh();
        }

        private void ComboBox_l_SelectedIndexChanged(object sender, EventArgs e)
        {
            Change_l();
            changeMxQyValue();
        }

        private void BuReset_Click(object sender, EventArgs e)
        {
            comboBox_l.SelectedIndex = 2;
            comboBox_q.SelectedIndex = 2;
            Change_l();
            changeMxQyValue();
            pictureBox1.Refresh();
        }

        private void TextBox_z1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                z1 = Convert.ToInt32(textBox_z1.Text);
                if (z1 >= 0 || z1 <= trackBar_z1.Maximum)
                {
                    trackBar_z1.Value = (int)z1;
                    changeMxQyValue();
                    SetParam();
                    pictureBox1.Refresh();
                    //createLines_z(pictureBox1.CreateGraphics(), (int)(a + b));
                }
            }
        }

        private void TextBox_z1_TextChanged(object sender, EventArgs e)
        {
            try
            {
                z1 = Convert.ToInt32(textBox_z1.Text);
                if (z1 < 0 || z1 > trackBar_z1.Maximum)
                {
                    textBox_z1.ForeColor = Color.Red;
                }
                else
                    textBox_z1.ForeColor = Color.Black;
            }
            catch
            {
                textBox_z1.ForeColor = Color.Red;
            }
        }

        private void TrackBar_z1_ValueChanged(object sender, EventArgs e)
        {
            textBox_z1.Text = trackBar_z1.Value.ToString();
            SetParam();
            pictureBox1.Refresh();
            //createLines_z(pictureBox1.CreateGraphics(), (int)(a + b));
        }


        private void TrackBar_z1_MouseUp(object sender, MouseEventArgs e)
        {
            changeMxQyValue();
        }

        private void TrackBar_c_MouseUp(object sender, MouseEventArgs e)
        {
            changeMxQyValue();
        }

        private void TrackBar_a_MouseUp(object sender, MouseEventArgs e)
        {
            changeMxQyValue();
        }
        double Qy;
        double Mx;
        private void changeMxQyValue()
        {
            SetParam();
            

            if ((int)(a + 0.5) + (int)(b + 0.5) + (int)(c + 0.5) == l && z1 + z2 == l)
            {
                for (int i = 0; i <= a; i++)
                {
                    if (z1 <= i)
                    {
                        Qy = (q * b * (b + 2 * c) / (2 * l)) / 1000;
                        labelQy.Text = string.Format("Qy = {0:N2} [кH]", Qy);
                        Mx = (q * b * (2 * c + b) * (z1) / (2 * l)) / 1000;
                        labelMx.Text = string.Format("Mx = {0:N2} [кН*м]", Mx);
                    }

                }
                for (int i = 0; i <= c; i++)
                {
                    if (z2 <= i)
                    {
                        Qy = (-q * b * (b + 2 * a) / (2 * l)) / 1000;
                        labelQy.Text = string.Format("Qy = {0:N2} [kH]", Qy);
                        Mx = (q * b * (2 * a + b) * ((l - z1)) / (2 * l)) / 1000;
                        labelMx.Text = string.Format("Mx = {0:N2} [кН*м]", Mx);
                    }
                }
                for (int i = 0; i <= a + b; i++)
                {
                    if (z1 <= i && z1 > a)
                    {
                        Qy = (q * b * ((b + 2 * c) / (2 * l) - (z1 - a) / (b))) / 1000;
                        labelQy.Text = string.Format("Qy = {0:N2} [kH]", Qy);
                        Mx = (q * b * b * ((2 * c + b) / (l) * ((z1) / (b)) - ((z1 - a) * (z1 - a) / (b * b))) / 2) / 1000;
                        labelMx.Text = string.Format("Mx = {0:N2} [кН*м]", Mx);
                    }
                }

            }
        }

        private void TextBox_c_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                c = Convert.ToInt32(textBox_c.Text);
                if (c >= 0 || c <= trackBar_c.Maximum)
                {
                    trackBar_c.Value = (int)c;
                    b = trackBar_a.Maximum - c - trackBar_a.Value + trackBar_c.Maximum;
                }
                pictureBox1.Refresh();
                changeMxQyValue();
            }
        }

        private void TextBox_a_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                a = Convert.ToInt32(textBox_a.Text);
                if (a >= 0 || a <= trackBar_a.Maximum)
                {
                    trackBar_a.Value = (int)a;
                    b = trackBar_a.Maximum - a - trackBar_c.Value + trackBar_c.Maximum;
                }      
                pictureBox1.Refresh();
                changeMxQyValue();
            }
        }

        private void TextBox_c_TextChanged(object sender, EventArgs e)
        {
            try
            {
                c = Convert.ToInt32(textBox_c.Text);
                if (c < 0 || c > trackBar_c.Maximum)
                {
                    textBox_c.ForeColor = Color.Red;
                }
                else
                    textBox_c.ForeColor = Color.Black;
            }
            catch
            {
                textBox_c.ForeColor = Color.Red;
            }
        }

        private void TextBox_a_TextChanged(object sender, EventArgs e)
        {
            try {
                a = Convert.ToInt32(textBox_a.Text);
                if (a < 0 || a > trackBar_a.Maximum)
                {
                    textBox_a.ForeColor = Color.Red;
                }
                else
                    textBox_a.ForeColor = Color.Black;
            }
            catch
            {
                textBox_a.ForeColor = Color.Red;
            }
        }

        private void Guna2TrackBar2_ValueChanged(object sender, EventArgs e)
        {
            c = trackBar_c.Value;
            b = trackBar_a.Maximum - trackBar_a.Value - c + trackBar_c.Maximum;

            textBox_c.Text = trackBar_c.Value.ToString();
            label_b.Text = String.Format("b = {0} [мм]", (int)b);

            pictureBox1.Refresh();
        }

        private void Guna2TrackBar1_ValueChanged(object sender, EventArgs e)
        {
            a = trackBar_a.Value;
            b = trackBar_a.Maximum - a - trackBar_c.Value + trackBar_c.Maximum;

            textBox_a.Text = trackBar_a.Value.ToString();
            label_b.Text = String.Format("b = {0} [мм]", (int)b);
            
            pictureBox1.Refresh();
        }
    }
}
