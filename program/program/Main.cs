﻿using Nevron.Nov.Text;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace program
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();

            guna2ImageButton1.Click += Guna2ImageButton1_Click;
            guna2ImageButton1.CheckedChanged += Guna2ImageButton1_CheckedChanged;
            guna2ImageButton2.Click += Guna2ImageButton2_Click;
            guna2ImageButton2.CheckedChanged += Guna2ImageButton2_CheckedChanged;
            guna2ImageButton3.Click += Guna2ImageButton3_Click;
            guna2ImageButton3.CheckedChanged += Guna2ImageButton3_CheckedChanged;
            guna2ImageButton4.Click += Guna2ImageButton4_Click;
            guna2ImageButton4.CheckedChanged += Guna2ImageButton4_CheckedChanged;
            guna2ImageButton5.Click += Guna2ImageButton5_Click;
            guna2ImageButton5.CheckedChanged += Guna2ImageButton5_CheckedChanged;
            guna2ImageButton6.Click += Guna2ImageButton6_Click;
            guna2ImageButton6.CheckedChanged += Guna2ImageButton6_CheckedChanged;
        }

        private void Guna2ImageButton6_CheckedChanged(object sender, EventArgs e)
        {
            if (guna2ImageButton6.Checked)
            {
                info1.BringToFront();
            }
            else
                info1.SendToBack();
        }

        private void Guna2ImageButton6_Click(object sender, EventArgs e)
        {
            guna2ImageButton6.Checked = !guna2ImageButton6.Checked;
        }

        private void Guna2ImageButton4_CheckedChanged(object sender, EventArgs e)
        {
            if (guna2ImageButton4.Checked)
            {
                history1.BringToFront();
            }
        }

        private void Guna2ImageButton3_CheckedChanged(object sender, EventArgs e)
        {
            if (guna2ImageButton3.Checked)
            {
                cutting1.BringToFront();
            }
        }
        private void Guna2ImageButton5_Click(object sender, EventArgs e)
        {
            guna2ImageButton5.Checked = !guna2ImageButton5.Checked;
        }

        private void Guna2ImageButton5_CheckedChanged(object sender, EventArgs e)
        {
            if (guna2ImageButton5.Checked)
            {
                user1.BringToFront();
            }
            else
                user1.SendToBack();
        }

        private void Guna2ImageButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (guna2ImageButton2.Checked)
            {
                calculate2.BringToFront();
            }
        }

        private void Guna2ImageButton1_CheckedChanged(object sender, EventArgs e)
        {
            if (guna2ImageButton1.Checked)
            {
                home1.BringToFront();
            }
        }

        private void Guna2ImageButton4_Click(object sender, EventArgs e)
        {
            guna2ImageButton4.Checked = true;
            guna2ImageButton3.Checked = false;
            guna2ImageButton2.Checked = false;
            guna2ImageButton1.Checked = false;

        }

        private void Guna2ImageButton3_Click(object sender, EventArgs e)
        {
            guna2ImageButton3.Checked = true;
            guna2ImageButton4.Checked = false;
            guna2ImageButton2.Checked = false;
            guna2ImageButton1.Checked = false;

        }

        private void Guna2ImageButton2_Click(object sender, EventArgs e)
        {
            guna2ImageButton2.Checked = true;
            guna2ImageButton3.Checked = false;
            guna2ImageButton4.Checked = false;
            guna2ImageButton1.Checked = false;            
        }

        private void Guna2ImageButton1_Click(object sender, EventArgs e)
        {
            guna2ImageButton1.Checked = true;
            guna2ImageButton3.Checked = false;
            guna2ImageButton2.Checked = false;
            guna2ImageButton4.Checked = false;
        }

    }
}
