﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace program
{
    public partial class User : UserControl
    {
        internal static Main form;

        public User()
        {
            InitializeComponent();

            guna2Button1.Click += Guna2Button1_Click;
            guna2Button3.Click += Guna2Button3_Click;
        }

        private void Guna2Button3_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void Guna2Button1_Click(object sender, EventArgs e)
        {
            form.Dispose();
            Fm fm = new Fm();
            fm.ShowDialog();
        }
    }
}
